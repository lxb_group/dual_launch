`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/12 12:28:35
// Design Name: 
// Module Name: MUX_NORMAL
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUX_NORMAL(
    input [31:0] instr_d1,
    input [1:0] grf_waddr_sel_d1,
    output reg [4:0] grf_waddr_d1,

    input [31:0] rf_rt_e,
    input [31:0] imm_ext_e,
    input alub_sel_e,
    output alub_e,

    input [31:0] dm_rdata_w,
    input [31:0] ao_w,
    input [31:0] pc8_w,
    input [31:0] cp0_reg_w,
    input wd3_sel_w,
    output reg [31:0] grf_wdata_w,

    input [31:0] alu_res_e,
    input [31:0] hilo_e,
    input ao_sel_e,
    output [31:0] ao_e
    );

    wire [4:0] rt_d1, rd_d1;
    assign {rt_d1, rd_d1} = instr_d1[20:11];

    always @(*) begin
        case (grf_waddr_sel_d1)
            2'b01: grf_waddr_d1 = rd_d1;
            2'b10: grf_waddr_d1 = 5'h1f;
            default: 
                grf_waddr_d1 = rt_d1;
        endcase
        case (wd3_sel_w)
            2'b01: grf_wdata_w = dm_rdata_w;
            2'b10: grf_wdata_w = pc8_w;
            2'b11: grf_wdata_w = cp0_reg_w;
            default:
                grf_wdata_w = ao_w;
        endcase
    end

    assign alub_e = alub_sel_e? imm_ext_e : rf_rt_e;
    assign ao_e = ao_sel_e? hilo_e : alu_res_e;

endmodule
