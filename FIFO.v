`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/05 19:59:36
// Design Name: 
// Module Name: FIFO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FIFO(
    input clk,
    input reset,
    input clr,
    input stall,
    input [31:0] src_pc,
    input [31:0] src_instr,
    input wen,
    input ren_A,
    input ren_B,

    output [31:0] src_instr_A,
    output [31:0] src_instr_B,
    output [31:0] src_pc_A,
    output [31:0] src_pc_B,
    output sig_full,
    output sig_empty,
    output sig_single
    );

    wire [4:0] next_head;
    wire [4:0] next_tail;
    wire [1:0] sum_of_out;  
    wire [1:0] sum_of_in;

    reg [63:0] fifo[0:`FIFO_SIZE - 1];
    reg [4:0] head, tail; 
    reg [4:0] number;
    integer i;

    assign sig_empty = (number == 5'd0);
    assign sig_full = (number == `FIFO_SIZE);
    assign sig_single = (number == 5'd1);
    assign sum_of_in = {1'b0, wen};
    assign sum_of_out = {1'b0, ren_A} + {1'b0, ren_B};

    assign next_head = (head + {3'b0, sum_of_out}) & 5'b01111;
    assign next_tail = (tail + {3'b0, sum_of_in}) & 5'b01111;
    assign {src_pc_A, src_instr_A} = fifo[head];
    assign {src_pc_B, src_instr_B} = fifo[(head + 5'd1) & (5'b01111)];

    always @(posedge clk) begin
        if (reset || clr) begin
            for(i = 0; i<`FIFO_SIZE ; i = i+1)
                fifo[i] = 64'd0;
            head <= 5'd0;
            tail <= 5'd0;
            number <= 5'd0;
        end
        else if (!stall) begin
            //base on a pre: if ren_A is 0, then ren_B is 0, mean if single launch, use A
            number <= number - sum_of_out + sum_of_in;
            head <= next_head;
            tail <= next_tail;
            if (wen) begin
                fifo[tail] <= {src_pc, src_instr};
            end
        end
    end

endmodule
