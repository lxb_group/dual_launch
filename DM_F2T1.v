`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/10 11:25:14
// Design Name: 
// Module Name: DM_F2T1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DM_F2T1(
    input sig_intreq,
    input except_AB,

    input ren_A,
    input wen_A,
    input [31:0] addr_A,
    input [31:0] wadata_A,
    input [1:0] s_sel_A,

    input ren_B,
    input wen_B,
    input [31:0] addr_B,
    input [31:0] wadata_B,
    input [1:0] s_sel_B,

    output reg ren,
    output reg wen,
    output reg [31:0] addr,
    output reg [31:0] wadata,
    output reg [1:0] dm_s_sel
    );

    wire stall_A;
    wire stall_B;

    assign stall_B = sig_intreq;
    assign stall_A = sig_intreq && (except_AB == `STREAM_A);

    always @(*) begin
        if (ren_B || wen_B) begin
            ren = ren_B && !stall_B;
            wen = wen_B && !stall_B;
            addr = addr_B;
            wadata = wadata_B;
            dm_s_sel = s_sel_B;
        end
        else begin
            ren = ren_A && !stall_A;
            wen = wen_A && !stall_A;
            addr = addr_A;
            wadata = wadata_A;
            dm_s_sel = s_sel_A;
        end
    end

endmodule
