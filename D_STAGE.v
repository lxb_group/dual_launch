`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/11 08:30:12
// Design Name: 
// Module Name: D_STAGE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module D_STAGE(
    input clk,
    input reset,
    input stall_bpu,
    input clr_bpu,
    input stall_d2,
    input clr_d2,

    input [31:0] FIFO_instr_A,
    input [31:0] FIFO_instr_B,
    input [31:0] FIFO_pc_A,
    input [31:0] FIFO_pc_B,
    input FIFO_empty,
    input FIFO_single,
    input res_cmp_e_A,

    input grf_wen_w_A,
    input [4:0] grf_waddr_w_A,
    input [31:0] grf_wdata_w_A,

    input grf_wen_w_B,
    input [4:0] grf_waddr_w_B,
    input [31:0] grf_wdata_w_B,

    output [31:0] pc_bp_error,
    output bp_error,
    output [1:0] npc_opt_d1_A,

    output ren_A,
    output ren_B,
    //grf
    output [31:0] grf_rdata1_d2_A,
    output [31:0] grf_rdata2_d2_A,
    output [31:0] grf_rdata1_d2_B,
    output [31:0] grf_rdata2_d2_B,
    //ext
    output [31:0] imm_ext_d2_A,
    output [31:0] imm_ext_d2_B,
    //reg_flow
    output [31:0] pc_d2_A,
    output [31:0] pc4_d2_A,
    output [31:0] pc8_d2_A,
    output [31:0] instr_d2_A,
    output alu_srcb_sel_d2_A,
    output [4:0] alu_opt_d2_A,
    output dm_ren_d2_A,
    output dm_wen_d2_A,
    output [1:0] wd3_sel_d2_A,
    output reg_wen_d2_A,
    output [44:0] data_hazard_d2_A,
    output [4:0] grf_waddr_d2_A,
    output [2:0] cmp_opt_d2_A,
    output [1:0] s_sel_d2_A,
    output [2:0] ld_ctr_d2_A,
    output md_d2_A,
    output [2:0] md_op_d2_A,
    output start_d2_A,
    output AO_SEL_d2_A,
    output BJ_d2_A,
    output eret_d2_A,
    output break_d2_A,
    output syscall_d2_A,
    output cp0_ren_d2_A,
    output cp0_wen_d2_A,
    output ri_d2_A,
    output overable_d2_A,
    output isir_d2_A,
    output JR_d2_A,

    output [31:0] pc_d2_B,
    output [31:0] pc4_d2_B,
    output [31:0] pc8_d2_B,
    output [31:0] instr_d2_B,
    output alu_srcb_sel_d2_B,
    output [4:0] alu_opt_d2_B,
    output dm_ren_d2_B,
    output dm_wen_d2_B,
    output [1:0] wd3_sel_d2_B,
    output reg_wen_d2_B,
    output [44:0] data_hazard_d2_B,
    output [4:0] grf_waddr_d2_B,
    output [2:0] cmp_opt_d2_B,
    output [1:0] s_sel_d2_B,
    output [2:0] ld_ctr_d2_B,
    output md_d2_B,
    output [2:0] md_op_d2_B,
    output start_d2_B,
    output AO_SEL_d2_B,
    output BJ_d2_B,
    output eret_d2_B,
    output break_d2_B,
    output syscall_d2_B,
    output cp0_ren_d2_B,
    output cp0_wen_d2_B,
    output ri_d2_B,
    output overable_d2_B,
    output isir_d2_B,
    output JR_d2_B
    );

    wire [31:0] instr_d1_A, instr_d1_B;
    wire [31:0] pc_d1_A, pc4_d1_A, pc8_d1_A;
    wire [31:0] pc_d1_B, pc4_d1_B, pc8_d1_B;

    assign {pc_d1_A, instr_d1_A} = ren_A? {FIFO_pc_A, FIFO_instr_A} : 64'd0;
    assign {pc_d1_B, instr_d1_B} = ren_B? {FIFO_pc_B, FIFO_instr_B} : 64'd0;
    assign pc4_d1_A = pc_d1_A + 4;
    assign pc4_d1_B = pc_d1_B + 4;
    assign pc8_d1_A = pc_d1_A + 8;
    assign pc8_d1_B = pc_d1_B + 8;

    wire branch_predict_A;

    wire [1:0] npc_opt_d1_A;
    wire [1:0] ext_opt_d1_A;
    wire alu_srcb_sel_d1_A;
    wire [4:0] alu_opt_d1_A;
    wire dm_ren_d1_A;
    wire dm_wen_d1_A;
    wire [1:0] a3_sel_d1_A;
    wire [1:0] wd3_sel_d1_A;
    wire reg_wen_d1_A;
    wire [2:0] cmp_opt_d1_A;
    wire [44:0] data_hazard_d1_A;
    wire BRANCH_d1_A;
    wire [1:0] s_sel_d1_A;
    wire [2:0] ld_ctr_d1_A;
    wire [2:0] md_op_d1_A;
    wire start_d1_A;
    wire md_d1_A;
    wire AO_SEL_d1_A;
    wire BJ_d1_A;
    wire eret_d1_A;
    wire break_d1_A;
    wire syscall_d1_A;
    wire cp0_ren_d1_A;
    wire cp0_wen_d1_A;
    wire ri_d1_A;
    wire overable_d1_A;
    wire JR_d1_A;
    wire st_ld_d1_A;
    
    DECODER decoder_A(
        .instr(FIFO_instr_A),
        .branch_predict(branch_predict_A),

        .npc_opt(npc_opt_d1_A),
        .ext_opt(ext_opt_d1_A),
        .alu_srcb_sel(alu_srcb_sel_d1_A),
        .alu_opt(alu_opt_d1_A),
        .dm_ren(dm_ren_d1_A),
        .dm_wen(dm_wen_d1_A),
        .a3_sel(a3_sel_d1_A),
        .wd3_sel(wd3_sel_d1_A),
        .reg_wen(reg_wen_d1_A),
        .cmp_opt(cmp_opt_d1_A),
        .data_hazard(data_hazard_d1_A),
        .BRANCH(BRANCH_d1_A),
        .s_sel(s_sel_d1_A),
        .ld_ctr(ld_ctr_d1_A),
        .md_op(md_op_d1_A),
        .start(start_d1_A),
        .MD(md_d1_A),
        .AO_SEL(AO_SEL_d1_A),
        .BJ(BJ_d1_A),
        .eret(eret_d1_A),
        .break(break_d1_A),
        .syscall(syscall_d1_A),
        .cp0_taken(cp0_ren_d1_A),
        .cp0_wen(cp0_wen_d1_A),
        .ri(ri_d1_A),
        .overable(overable_d1_A),
        .JR(JR_d1_A),
        .st_ld(st_ld_d1_A)
    );

    wire [1:0] npc_opt_d1_B;
    wire [1:0] ext_opt_d1_B;
    wire alu_srcb_sel_d1_B;
    wire [4:0] alu_opt_d1_B;
    wire dm_ren_d1_B;
    wire dm_wen_d1_B;
    wire [1:0] a3_sel_d1_B;
    wire [1:0] wd3_sel_d1_B;
    wire reg_wen_d1_B;
    wire [2:0] cmp_opt_d1_B;
    wire [44:0] data_hazard_d1_B;
    wire BRANCH_d1_B;
    wire [1:0] s_sel_d1_B;
    wire [2:0] ld_ctr_d1_B;
    wire [2:0] md_op_d1_B;
    wire start_d1_B;
    wire md_d1_B;
    wire AO_SEL_d1_B;
    wire BJ_d1_B;
    wire eret_d1_B;
    wire break_d1_B;
    wire syscall_d1_B;
    wire cp0_ren_d1_B;
    wire cp0_wen_d1_B;
    wire ri_d1_B;
    wire overable_d1_B;
    wire JR_d1_B;
    wire st_ld_d1_B;

    DECODER decoder_B(
        .instr(FIFO_instr_B),
        .branch_predict(1'b0),      //因为分支指令不会出现在B

        .npc_opt(npc_opt_d1_B),
        .ext_opt(ext_opt_d1_B),
        .alu_srcb_sel(alu_srcb_sel_d1_B),
        .alu_opt(alu_opt_d1_B),
        .dm_ren(dm_ren_d1_B),
        .dm_wen(dm_wen_d1_B),
        .a3_sel(a3_sel_d1_B),
        .wd3_sel(wd3_sel_d1_B),
        .reg_wen(reg_wen_d1_B),
        .cmp_opt(cmp_opt_d1_B),
        .data_hazard(data_hazard_d1_B),
        .BRANCH(BRANCH_d1_B),
        .s_sel(s_sel_d1_B),
        .ld_ctr(ld_ctr_d1_B),
        .md_op(md_op_d1_B),
        .start(start_d1_B),
        .MD(md_d1_B),
        .AO_SEL(AO_SEL_d1_B),
        .BJ(BJ_d1_B),
        .eret(eret_d1_B),
        .break(break_d1_B),
        .syscall(syscall_d1_B),
        .cp0_taken(cp0_ren_d1_B),
        .cp0_wen(cp0_wen_d1_B),
        .ri(ri_d1_B),
        .overable(overable_d1_B),
        .JR(JR_d1_B),
        .st_ld(st_ld_d1_B)
    );

    JUDGER judger(
        .data_A(data_hazard_d1_A),
        .data_B(data_hazard_d1_B),
        .sig_stall_d2(stall_d2),
        .sig_clr_d2(clr_d2),
        .FIFO_empty(FIFO_empty),
        .FIFO_single(FIFO_single),
        .md_A(md_d1_A),
        .md_B(md_d1_B),
        .jb_B(BJ_d1_B),
        .eret_B(eret_d1_B),
        .sl_A(st_ld_d1_A),
        .sl_B(st_ld_d1_B),
        .ren_A(ren_A),
        .ren_B(ren_B)
    );

    BPU bpu(
        .clk(clk),
        .reset(reset),
        .clr(clr_bpu),
        .stall(stall_bpu),
        .sig_br_d1_A(BRANCH_d1_A && ren_A),         //发射A且A是分支指�???
        .src_res_cmp_E_A(res_cmp_e_A),
        .src_pc4_d1_A(pc4_d1_A),
        .src_ir16_d1_A(instr_d1_A[15:0]),
        .src_bp(branch_predict_A),
        .src_pc_bp_e(pc_bp_error),
        .sig_br_error(bp_error)
    );
    
    wire [4:0] grf_waddr_d1_A;
    wire [1:0] ext_opt_d2_A;
    MUX_NORMAL mux_nom_d_A(
        .instr_d1(instr_d1_A),
        .grf_waddr_sel_d1(a3_sel_d1_A),
        .grf_waddr_d1(grf_waddr_d1_A)
    );

    REGS_FLOW D1_D2_A(
        .clk(clk),
        .reset(reset),
        .stall(stall_d2),
        .clr(clr_d2),
        .pc_I(pc_d1_A),
        .pc4_I(pc4_d1_A),
        .pc8_I(pc8_d1_A),
        .ir_I(instr_d1_A),
        .ext_opt_I(ext_opt_d1_A),
        .alub_sel_I(alu_srcb_sel_d1_A),
        .alu_ctr_I(alu_opt_d1_A),
        .dm_ren_I(dm_ren_d1_A),
        .dm_wen_I(dm_wen_d1_A),
        .wd3_sel_I(wd3_sel_d1_A),
        .reg_wen_I(reg_wen_d1_A),
        .data_hazard_I(data_hazard_d1_A),
        .grf_waddr_I(grf_waddr_d1_A),
        .cmp_ctr_I(cmp_opt_d1_A),
        .s_sel_I(s_sel_d1_A),
        .ld_ctr_I(ld_ctr_d1_A),
        .md_I(md_d1_A),
        .md_op_I(md_op_d1_A),
        .start_I(start_d1_A),
        .ao_sel_I(AO_SEL_d1_A),
        .bj_I(BJ_d1_A),
        .eret_I(eret_d1_A),
        .break_I(break_d1_A),
        .syscall_I(syscall_d1_A),
        .cp0_ren_I(cp0_ren_d1_A),
        .cp0_wen_I(cp0_wen_d1_A),
        .ri_I(ri_d1_A),
        .overable_I(overable_d1_A),
        .isir_I(ren_A),
        .jr_I(JR_d1_A),

        .pc_O(pc_d2_A),
        .pc4_O(pc4_d2_A),
        .pc8_O(pc8_d2_A),
        .ir_O(instr_d2_A),
        .ext_opt_O(ext_opt_d2_A),
        .alub_sel_O(alu_srcb_sel_d2_A),
        .alu_ctr_O(alu_opt_d2_A),
        .dm_ren_O(dm_ren_d2_A),
        .dm_wen_O(dm_wen_d2_A),
        .wd3_sel_O(wd3_sel_d2_A),
        .reg_wen_O(reg_wen_d2_A),
        .data_hazard_O(data_hazard_d2_A),
        .grf_waddr_O(grf_waddr_d2_A),
        .cmp_ctr_O(cmp_opt_d2_A),
        .s_sel_O(s_sel_d2_A),
        .ld_ctr_O(ld_ctr_d2_A),
        .md_O(md_d2_A),
        .md_op_O(md_op_d2_A),
        .start_O(start_d2_A),
        .ao_sel_O(AO_SEL_d2_A),
        .bj_O(BJ_d2_A),
        .eret_O(eret_d2_A),
        .break_O(break_d2_A),
        .syscall_O(syscall_d2_A),
        .cp0_ren_O(cp0_ren_d2_A),
        .cp0_wen_O(cp0_wen_d2_A),
        .ri_O(ri_d2_A),
        .overable_O(overable_d2_A),
        .isir_O(isir_d2_A),
        .jr_O(JR_d2_A)
    );

    wire [4:0] grf_waddr_d1_B;
    wire [1:0] ext_opt_d2_B;

    MUX_NORMAL mux_nom_d_B(
        .instr_d1(instr_d1_B),
        .grf_waddr_sel_d1(a3_sel_d1_B),
        .grf_waddr_d1(grf_waddr_d1_B)
    );

    REGS_FLOW D1_D2_B(
        .clk(clk),
        .reset(reset),
        .stall(stall_d2),          
        .clr(clr_d2),
        .pc_I(pc_d1_B),
        .pc4_I(pc4_d1_B),
        .pc8_I(pc8_d1_B),
        .ir_I(instr_d1_B),
        .ext_opt_I(ext_opt_d1_B),
        .alub_sel_I(alu_srcb_sel_d1_B),
        .alu_ctr_I(alu_opt_d1_B),
        .dm_ren_I(dm_ren_d1_B),
        .dm_wen_I(dm_wen_d1_B),
        .wd3_sel_I(wd3_sel_d1_B),
        .reg_wen_I(reg_wen_d1_B),
        .data_hazard_I(data_hazard_d1_B),
        .grf_waddr_I(grf_waddr_d1_B),
        .cmp_ctr_I(cmp_opt_d1_B),
        .s_sel_I(s_sel_d1_B),
        .ld_ctr_I(ld_ctr_d1_B),
        .md_I(md_d1_B),
        .md_op_I(md_op_d1_B),
        .start_I(start_d1_B),
        .ao_sel_I(AO_SEL_d1_B),
        .bj_I(BJ_d1_B),
        .eret_I(eret_d1_B),
        .break_I(break_d1_B),
        .syscall_I(syscall_d1_B),
        .cp0_ren_I(cp0_ren_d1_B),
        .cp0_wen_I(cp0_wen_d1_B),
        .ri_I(ri_d1_B),
        .overable_I(overable_d1_B),
        .isir_I(ren_B),
        .jr_I(JR_d1_B),

        .pc_O(pc_d2_B),
        .pc4_O(pc4_d2_B),
        .pc8_O(pc8_d2_B),
        .ir_O(instr_d2_B),
        .ext_opt_O(ext_opt_d2_B),
        .alub_sel_O(alu_srcb_sel_d2_B),
        .alu_ctr_O(alu_opt_d2_B),
        .dm_ren_O(dm_ren_d2_B),
        .dm_wen_O(dm_wen_d2_B),
        .wd3_sel_O(wd3_sel_d2_B),
        .reg_wen_O(reg_wen_d2_B),
        .data_hazard_O(data_hazard_d2_B),
        .grf_waddr_O(grf_waddr_d2_B),
        .cmp_ctr_O(cmp_opt_d2_B),
        .s_sel_O(s_sel_d2_B),
        .ld_ctr_O(ld_ctr_d2_B),
        .md_O(md_d2_B),
        .md_op_O(md_op_d2_B),
        .start_O(start_d2_B),
        .ao_sel_O(AO_SEL_d2_B),
        .bj_O(BJ_d2_B),
        .eret_O(eret_d2_B),
        .break_O(break_d2_B),
        .syscall_O(syscall_d2_B),
        .cp0_ren_O(cp0_ren_d2_B),
        .cp0_wen_O(cp0_wen_d2_B),
        .ri_O(ri_d2_B),
        .overable_O(overable_d2_B),
        .isir_O(isir_d2_B),
        .jr_O(JR_d2_B)
    );

    EXT ext_A(
        .src_imm(instr_d2_A[15:0]),
        .sig_ext_opt(ext_opt_d2_A),
        .imm_ext(imm_ext_d2_A)
    );

    EXT ext_B(
        .src_imm(instr_d2_B[15:0]),
        .sig_ext_opt(ext_opt_d2_B),
        .imm_ext(imm_ext_d2_B)
    );

    GRF grf(
        .clk(clk),
        .reset(reset),
        .src_raddr1_A(instr_d2_A[25:21]),
        .src_raddr2_A(instr_d2_A[20:16]),
        .src_rdata1_A(grf_rdata1_d2_A),
        .src_rdata2_A(grf_rdata2_d2_A),
        .src_raddr1_B(instr_d2_B[25:21]),
        .src_raddr2_B(instr_d2_B[20:16]),
        .src_rdata1_B(grf_rdata1_d2_B),
        .src_rdata2_B(grf_rdata2_d2_B),
        .sig_we_A(grf_wen_w_A),
        .src_waddr_A(grf_waddr_w_A),
        .src_wdata_A(grf_wdata_w_A),
        .sig_we_B(grf_wen_w_B),
        .src_waddr_B(grf_waddr_w_B),
        .src_wdata_B(grf_wdata_w_B)
    );

endmodule
