`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/06 16:36:18
// Design Name: 
// Module Name: HAZARD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module HAZARD(
    input [44:0] Data_E_A,
    input [44:0] Data_M1_A,
    input [44:0] Data_M2_A,
    input [44:0] Data_W_A,
    input [44:0] Data_E_B,
    input [44:0] Data_M1_B,
    input [44:0] Data_M2_B,
    input [44:0] Data_W_B,

    input BUSY_E,
    input MD_E_A,
    input MD_E_B,

    output [4:0] sig_rs_e_A,
    output [4:0] sig_rt_e_A,
    output [4:0] sig_rs_e_B,
    output [4:0] sig_rt_e_B,
    output [4:0] sig_rt_m1_A,
    output [4:0] sig_rt_m1_B,
    
    output hazard_stall
    );

    wire [4:0] RS_E_A, RT_E_A, Tuse1_E_A, Tuse2_E_A, grf1_E_A, grf2_E_A, grfchange_E_A, Tnew_E_A, WD3_E_A;
    wire [4:0] RS_M1_A, RT_M1_A, Tuse1_M1_A, Tuse2_M1_A, grf1_M1_A, grf2_M1_A, grfchange_M1_A, Tnew_M1_A, WD3_M1_A;
    wire [4:0] RS_M2_A, RT_M2_A, Tuse1_M2_A, Tuse2_M2_A, grf1_M2_A, grf2_M2_A, grfchange_M2_A, Tnew_M2_A, WD3_M2_A;
    wire [4:0] RS_W_A, RT_W_A, Tuse1_W_A, Tuse2_W_A, grf1_W_A, grf2_W_A, grfchange_W_A, Tnew_W_A, WD3_W_A;

    wire [4:0] RS_E_B, RT_E_B, Tuse1_E_B, Tuse2_E_B, grf1_E_B, grf2_E_B, grfchange_E_B, Tnew_E_B, WD3_E_B;
    wire [4:0] RS_M1_B, RT_M1_B, Tuse1_M1_B, Tuse2_M1_B, grf1_M1_B, grf2_M1_B, grfchange_M1_B, Tnew_M1_B, WD3_M1_B;
    wire [4:0] RS_M2_B, RT_M2_B, Tuse1_M2_B, Tuse2_M2_B, grf1_M2_B, grf2_M2_B, grfchange_M2_B, Tnew_M2_B, WD3_M2_B;
    wire [4:0] RS_W_B, RT_W_B, Tuse1_W_B, Tuse2_W_B, grf1_W_B, grf2_W_B, grfchange_W_B, Tnew_W_B, WD3_W_B;

    assign {RS_E_A, RT_E_A, Tuse1_E_A, Tuse2_E_A, grf1_E_A, grf2_E_A, grfchange_E_A, Tnew_E_A, WD3_E_A} = Data_E_A;
    assign {RS_M1_A, RT_M1_A, Tuse1_M1_A, Tuse2_M1_A, grf1_M1_A, grf2_M1_A, grfchange_M1_A, Tnew_M1_A, WD3_M1_A} = Data_M1_A;
    assign {RS_M2_A, RT_M2_A, Tuse1_M2_A, Tuse2_M2_A, grf1_M2_A, grf2_M2_A, grfchange_M2_A, Tnew_M2_A, WD3_M2_A} = Data_M2_A;
    assign {RS_W_A, RT_W_A, Tuse1_W_A, Tuse2_W_A, grf1_W_A, grf2_W_A, grfchange_W_A, Tnew_W_A, WD3_W_A} = Data_W_A;

    assign {RS_E_B, RT_E_B, Tuse1_E_B, Tuse2_E_B, grf1_E_B, grf2_E_B, grfchange_E_B, Tnew_E_B, WD3_E_B} = Data_E_B;
    assign {RS_M1_B, RT_M1_B, Tuse1_M1_B, Tuse2_M1_B, grf1_M1_B, grf2_M1_B, grfchange_M1_B, Tnew_M1_B, WD3_M1_B} = Data_M1_B;
    assign {RS_M2_B, RT_M2_B, Tuse1_M2_B, Tuse2_M2_B, grf1_M2_B, grf2_M2_B, grfchange_M2_B, Tnew_M2_B, WD3_M2_B} = Data_M2_B;
    assign {RS_W_B, RT_W_B, Tuse1_W_B, Tuse2_W_B, grf1_W_B, grf2_W_B, grfchange_W_B, Tnew_W_B, WD3_W_B} = Data_W_B;

    wire stall_M1_A, stall_M1_B, stall_M2_A, stall_M2_B, stall_MD;

    //因为use: E_PIPE、M1_PIPE, 但是change: D2_PIPE、M1_PIPE、W_PIPE, 且不同级的话，D2和M1时change的显然可以转发，而W_PIPE只有差两级才能转发给M1，差三级转发给E。
    assign stall_M1_A = grfchange_M1_A && (Tnew_M1_A == `W_PIPE || Tnew_M1_A == `M2_PIPE) && 
                ((grfchange_M1_A == grf1_E_A) || (grfchange_M1_A == grf2_E_A) || (grfchange_M1_A == grf1_E_B) || (grfchange_M1_A == grf2_E_B));
    assign stall_M1_B = grfchange_M1_B && (Tnew_M1_B == `W_PIPE || Tnew_M1_B == `M2_PIPE) && 
                ((grfchange_M1_B == grf1_E_A) || (grfchange_M1_B == grf2_E_A) || (grfchange_M1_B == grf1_E_B) || (grfchange_M1_B == grf2_E_B));
    assign stall_M2_A = grfchange_M2_A && (Tnew_M2_A == `W_PIPE) && (Tuse1_E_A == `E_PIPE && (grfchange_M2_A == grf1_E_A) && (grfchange_M2_A == grf2_E_A)) && 
                (Tuse1_E_B == `E_PIPE && (grfchange_M2_A == grf1_E_B) && (grfchange_M2_A == grf2_E_B));
    assign stall_M2_B = grfchange_M2_B && (Tnew_M2_B == `W_PIPE) && (Tuse1_E_A == `E_PIPE && (grfchange_M2_B == grf1_E_A) && (grfchange_M2_B == grf2_E_A)) && 
                (Tuse1_E_B == `E_PIPE && (grfchange_M2_B == grf1_E_B) && (grfchange_M2_B == grf2_E_B));

    assign stall_MD = (BUSY_E) && (MD_E_A || MD_E_B);
    assign hazard_stall = stall_M1_A || stall_M1_B || stall_M2_A || stall_M2_B || stall_MD;

    assign sig_rs_e_A = (grfchange_M1_B != 5'd0) && (grfchange_M1_B == RS_E_A) && (WD3_M1_B == `AO)?    5'd1:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RS_E_A) && (WD3_M1_A == `AO)?    5'd2:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RS_E_A) && (WD3_M1_A == `PC8)?   5'd3:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RS_E_A) && (WD3_M2_B == `AO)?    5'd4:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_A) && (WD3_M2_A == `AO)?    5'd5:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_A) && (WD3_M2_A == `PC8)?   5'd6:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RS_E_A) && (WD3_M2_B == `CP0)?   5'd7:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_A) && (WD3_M2_A == `CP0)?   5'd8:
                        (grfchange_W_B != 5'd0) && (grfchange_W_B == RS_E_A)?                           5'd9:
                        (grfchange_W_A != 5'd0) && (grfchange_W_A == RS_E_A)?                           5'd10: 5'd0;

    assign sig_rs_e_B = (grfchange_E_A != 5'd0) && (grfchange_E_A == RS_E_B) && (WD3_E_A == `PC8)?      5'd1:
                        (grfchange_M1_B != 5'd0) && (grfchange_M1_B == RS_E_B) && (WD3_M1_B == `AO)?    5'd2:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RS_E_B) && (WD3_M1_A == `AO)?    5'd3:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RS_E_B) && (WD3_M1_A == `PC8)?   5'd4:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RS_E_B) && (WD3_M2_B == `AO)?    5'd5:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_B) && (WD3_M2_A == `AO)?    5'd6:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_B) && (WD3_M2_A == `PC8)?   5'd7:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RS_E_B) && (WD3_M2_B == `CP0)?   5'd8:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RS_E_B) && (WD3_M2_A == `CP0)?   5'd9:
                        (grfchange_W_B != 5'd0) && (grfchange_W_B == RS_E_B)?                           5'd10:
                        (grfchange_W_A != 5'd0) && (grfchange_W_A == RS_E_B)?                           5'd11: 5'd0;

    assign sig_rt_e_A = (grfchange_M1_B != 5'd0) && (grfchange_M1_B == RT_E_A) && (WD3_M1_B == `AO)?    5'd1:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_A) && (WD3_M1_A == `AO)?    5'd2:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_A) && (WD3_M1_A == `PC8)?   5'd3:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_A) && (WD3_M2_B == `AO)?    5'd4:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `AO)?    5'd5:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `PC8)?   5'd6:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_A) && (WD3_M2_B == `CP0)?   5'd7:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `CP0)?   5'd8:
                        (grfchange_W_B != 5'd0) && (grfchange_W_B == RT_E_A)?                           5'd9:
                        (grfchange_W_A != 5'd0) && (grfchange_W_A == RT_E_A)?                           5'd10: 5'd0;


    assign sig_rt_e_B = (grfchange_E_A != 5'd0) && (grfchange_E_A == RT_E_B) && (WD3_E_A == `PC8)?      5'd1:
                        (grfchange_M1_B != 5'd0) && (grfchange_M1_B == RT_E_B) && (WD3_M1_B == `AO)?    5'd2:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_B) && (WD3_M1_A == `AO)?    5'd3:
                        (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_B) && (WD3_M1_A == `PC8)?   5'd4:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_B) && (WD3_M2_B == `AO)?    5'd5:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `AO)?    5'd6:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `PC8)?   5'd7:
                        (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_B) && (WD3_M2_B == `CP0)?   5'd8:
                        (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `CP0)?   5'd9:
                        (grfchange_W_B != 5'd0) && (grfchange_W_B == RT_E_B)?                           5'd10:
                        (grfchange_W_A != 5'd0) && (grfchange_W_A == RT_E_B)?                           5'd11: 5'd0;

    assign sig_rt_m1_A = (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_A) && (WD3_M2_B == `AO)?   5'd1:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `AO)?   5'd2:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `PC8)?  5'd3:
                         (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_A) && (WD3_M2_B == `CP0)?  5'd4:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_A) && (WD3_M2_A == `CP0)?  5'd5:
                         (grfchange_W_B != 5'd0) && (grfchange_W_B == RT_E_A)?                          5'd6:
                         (grfchange_W_A != 5'd0) && (grfchange_W_A == RT_E_A)?                          5'd7: 5'd0;

    assign sig_rt_m1_B = (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_B) && (WD3_M1_A == `AO)?   5'd1:
                         (grfchange_M1_A != 5'd0) && (grfchange_M1_A == RT_E_B) && (WD3_M1_A == `PC8)?  5'd2:
                         (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_B) && (WD3_M2_B == `AO)?   5'd3:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `AO)?   5'd4:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `PC8)?  5'd5:
                         (grfchange_M2_B != 5'd0) && (grfchange_M2_B == RT_E_B) && (WD3_M2_B == `CP0)?  5'd6:
                         (grfchange_M2_A != 5'd0) && (grfchange_M2_A == RT_E_B) && (WD3_M2_A == `CP0)?  5'd7:
                         (grfchange_W_B != 5'd0) && (grfchange_W_B == RT_E_B)?                          5'd8:
                         (grfchange_W_A != 5'd0) && (grfchange_W_A == RT_E_B)?                          5'd9: 5'd0;

endmodule
