module TB_GRF(
	);

	reg clk;
	reg reset;
	reg [4:0] src_raddr1_A;
	reg [4:0] src_raddr2_A;
	reg [4:0] src_raddr1_B;
	reg [4:0] src_raddr2_B;
	reg sig_we_A;
	reg [4:0] src_waddr_A;
	reg [31:0] src_wdata_A;
	reg sig_we_B;
	reg [4:0] src_waddr_B;
	reg [31:0] src_wdata_B;

	wire [31:0] src_rdata1_A;
	wire [31:0] src_rdata2_A;
	wire [31:0] src_rdata1_B;
	wire [31:0] src_rdata2_B;

	GRF GRF_test(
		.clk(clk),
		.reset(reset),
		.src_raddr1_A(src_raddr1_A),
		.src_raddr2_A(src_raddr2_A),
		.src_raddr1_B(src_raddr1_B),
		.src_raddr2_B(src_raddr2_B),
		.sig_we_A(sig_we_A),
		.src_waddr_A(src_waddr_A),
		.src_wdata_A(src_wdata_A),
		.sig_we_B(sig_we_B),
		.src_waddr_B(src_waddr_B),
		.src_wdata_B(src_wdata_B),
		.src_rdata1_A(src_rdata1_A),
		.src_rdata2_A(src_rdata2_A),
		.src_rdata1_B(src_rdata1_B),
		.src_rdata2_B(src_rdata2_B)
	);

	initial begin
		clk = 0;
		reset = 1;

		#20
		reset = 0;

		src_raddr1_A = 5;
		src_raddr2_A = 6;
		src_raddr1_B = 7;
		src_raddr2_B = 8;
		sig_we_A = 1;
		src_waddr_A = 5;
		src_wdata_A = 32'h541;
		sig_we_B = 1;
		src_waddr_B = 5;
		src_wdata_B = 32'h548;
		#10 
		src_waddr_A = 6;
		src_waddr_B = 6;
		#10
		src_waddr_A = 7;
		src_waddr_B = 7;
		#10
		src_waddr_A = 8;
		src_waddr_B = 8;
		#10
		src_waddr_A = 7;
		#10
		src_waddr_B = 6;
	end
	
	always #5 clk = ~clk;

endmodule
