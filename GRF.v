`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/03 10:31:25
// Design Name: 
// Module Name: GRF
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module GRF(
	input clk,
	input reset,
	//A read grf
	input [4:0] src_raddr1_A,
	input [4:0] src_raddr2_A,
	output [31:0] src_rdata1_A,
	output [31:0] src_rdata2_A,
	//B read grf
	input [4:0] src_raddr1_B,
	input [4:0] src_raddr2_B,
	output [31:0] src_rdata1_B,
	output [31:0] src_rdata2_B,
	//A write grf
	input sig_we_A,
	input [4:0] src_waddr_A,
	input [31:0] src_wdata_A,
	//B write  grf
	input sig_we_B,
	input [4:0] src_waddr_B,
	input [31:0] src_wdata_B
    );

    reg [31:0] grf[0:31];
	integer i;
	
	wire a1a, a2a, a1b, a2b, b1a, b2a, b1b, b2b;

	assign a1a = (src_raddr1_A != 0) && src_raddr1_A == src_waddr_A && sig_we_A;
	assign a2a = (src_raddr2_A != 0) && src_raddr2_A == src_waddr_A && sig_we_A;
	assign a1b = (src_raddr1_A != 0) && src_raddr1_A == src_waddr_B && sig_we_B;
	assign a2b = (src_raddr2_A != 0) && src_raddr2_A == src_waddr_B && sig_we_B;
	
	assign b1a = (src_raddr1_B != 0) && src_raddr1_B == src_waddr_A && sig_we_A;
	assign b2a = (src_raddr2_B != 0) && src_raddr2_B == src_waddr_A && sig_we_A;
	assign b1b = (src_raddr1_B != 0) && src_raddr1_B == src_waddr_B && sig_we_B;
	assign b2b = (src_raddr2_B != 0) && src_raddr2_B == src_waddr_B && sig_we_B;

    assign src_rdata1_A = a1b? src_wdata_B : (a1a? src_wdata_A : grf[src_raddr1_A]);
	assign src_rdata2_A = a2b? src_wdata_B : (a2a? src_wdata_A : grf[src_raddr2_A]);

    assign src_rdata1_B = b1b? src_wdata_B : (b1a? src_wdata_A : grf[src_raddr1_B]);
	assign src_rdata2_B = b2b? src_wdata_B : (b2a? src_wdata_A : grf[src_raddr2_B]);

    always @(posedge clk)
        if(reset) 
			for (i = 0; i<=31 ; i = i+1) 
				grf[i] <= 0;
        else begin
            if(sig_we_B && src_waddr_B != 0)
				grf[src_waddr_B] <= src_wdata_B;
            if(sig_we_A && src_waddr_A != 0 && !(src_waddr_A == src_waddr_B && sig_we_B))
				grf[src_waddr_A] <= src_wdata_A;
		end

endmodule

