`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/10 08:55:39
// Design Name: 
// Module Name: REGS_FLOW
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module REGS_FLOW(
    input clk,
    input reset,
    input stall,
    input clr,
    input [31:0] pc_I,
    input [31:0] pc4_I,
    input [31:0] pc8_I,
    input [31:0] ir_I,
    input [31:0] rf_rs_I,
    input [31:0] rf_rt_I,
    input [31:0] imm_ext_I,
    input [31:0] ao_I,
    input [31:0] rdata_I,
    //ctr
    input [1:0] ext_opt_I,
    input alub_sel_I,
    input [4:0] alu_ctr_I,
    input dm_ren_I,
    input dm_wen_I,
    input [1:0] wd3_sel_I,
    input reg_wen_I,
    input [44:0] data_hazard_I,
    input [4:0] grf_waddr_I,
    input [2:0] cmp_ctr_I,
    input [1:0] s_sel_I,
    input [2:0] ld_ctr_I,
    input md_I,
    input [2:0] md_op_I, 
    input start_I,
    input ao_sel_I,
    input bj_I,
    input eret_I,
    input break_I,
    input syscall_I,
    input cp0_ren_I,
    input cp0_wen_I,
    input ri_I,
    input overable_I,
    input over_I,
    input isir_I,
    input jr_I,
    input [31:0] cp0_grf_I,
    
    output reg [31:0] pc_O,
    output reg [31:0] pc4_O,
    output reg [31:0] pc8_O,
    output reg [31:0] ir_O,
    output reg [31:0] rf_rs_O,
    output reg [31:0] rf_rt_O,
    output reg [31:0] imm_ext_O,
    output reg [31:0] ao_O,
    output reg [31:0] rdata_O,
    //ctr
    output reg [1:0] ext_opt_O,
    output reg alub_sel_O,
    output reg [4:0] alu_ctr_O,
    output reg dm_ren_O,
    output reg dm_wen_O,
    output reg [1:0] wd3_sel_O,
    output reg reg_wen_O,
    output reg [44:0] data_hazard_O,
    output reg [4:0] grf_waddr_O,
    output reg [2:0] cmp_ctr_O,
    output reg [1:0] s_sel_O,
    output reg [2:0] ld_ctr_O,
    output reg md_O,
    output reg [2:0] md_op_O,
    output reg start_O,
    output reg ao_sel_O,
    output reg bj_O,
    output reg eret_O,
    output reg break_O,
    output reg syscall_O,
    output reg cp0_ren_O,
    output reg cp0_wen_O,
    output reg ri_O,
    output reg overable_O,
    output reg over_O,
    output reg isir_O,
    output reg jr_O,
    output reg [31:0] cp0_grf_O
    );

     always @(posedge clk)
        if(reset || clr) begin
            pc_O <= 32'hbfc0_0000;
            pc4_O <= 32'hbfc0_0000;
            pc8_O <= 32'hbfc0_0000;
            ir_O <= 32'd0;
            rf_rs_O <= 32'd0;
            rf_rt_O <= 32'd0;
            imm_ext_O <= 32'd0;
            ao_O <= 32'd0;
            rdata_O <= 32'd0;
            alub_sel_O <= 0;  
            alu_ctr_O <= 0;  
            dm_ren_O <= 0;    
            dm_wen_O <= 0;    
            wd3_sel_O <= 0;  
            reg_wen_O <= 0;  
            data_hazard_O <= 45'd0; 
            grf_waddr_O <= 5'd0;
            cmp_ctr_O <= 3'd0;
            s_sel_O <= 2'd0;
            ld_ctr_O <= 3'd0;
            md_O <= 0;
            md_op_O <= 3'd0;
            start_O <= 1'd0;
            ao_sel_O <= 1'd0;
            bj_O <= 1'b0;
            eret_O <= 1'b0;
            break_O <= 1'b0;
            syscall_O <= 1'b0;
            cp0_ren_O <= 1'b0;
            cp0_wen_O <= 1'b0;
            ri_O <= 1'b0;
            overable_O <= 1'b0;
            over_O <= 1'b0;     
            isir_O <= 1'b0; 
            jr_O <= 1'b0;  
            cp0_grf_O <= 0;
            ext_opt_O <= 0;
        end
        else if(!stall) begin
            pc_O <= pc_I;
            pc4_O <= pc4_I;
            pc8_O <= pc8_I;
            ir_O <= ir_I;
            rf_rs_O <= rf_rs_I;
            rf_rt_O <= rf_rt_I;
            imm_ext_O <= imm_ext_I;
            ao_O <= ao_I;
            rdata_O <= rdata_I;
            alub_sel_O <= alub_sel_I;  
            alu_ctr_O <= alu_ctr_I;  
            dm_ren_O <= dm_ren_I;    
            dm_wen_O <= dm_wen_I;    
            wd3_sel_O <= wd3_sel_I;
            reg_wen_O <= reg_wen_I;  
            data_hazard_O <=data_hazard_I; 
            grf_waddr_O <= grf_waddr_I;   
            cmp_ctr_O <= cmp_ctr_I;
            s_sel_O <= s_sel_I;
            ld_ctr_O <= ld_ctr_I;
            md_O <= md_I;
            md_op_O <= md_op_I;
            start_O <= start_I;
            ao_sel_O <= ao_sel_I;
            bj_O <= bj_I;
            eret_O <= eret_I;
            break_O <= break_I;
            syscall_O <= syscall_I;
            cp0_ren_O <= cp0_ren_I;
            cp0_wen_O <= cp0_wen_I;
            ri_O <= ri_I;
            overable_O <= overable_I;
            over_O <= over_I;     
            isir_O <= isir_I;   
            jr_O <= jr_I;
            cp0_grf_O <= cp0_grf_I;
            ext_opt_O <= ext_opt_I;
        end

endmodule
