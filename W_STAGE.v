`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/14 10:53:19
// Design Name: 
// Module Name: W_STAGE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module W_STAGE(
    input clk,
    input reset,
    input stall_w,
    input clr_w,

    input [31:0] pc_m2_A,
    input [31:0] pc4_m2_A,
    input [31:0] pc8_m2_A,
    input [31:0] ir_m2_A,
    input [31:0] ao_m2_A,
    input [31:0] dm_rdata_m2_A,
    input [1:0] wd3_sel_m2_A,
    input reg_wen_m2_A,
    input [44:0] data_hazard_m2_A,
    input [4:0] grf_waddr_m2_A,
    input isir_m2_A,
    input [31:0] cp0_grf_m2_A,
    output [31:0] data_hazard_w_A,
    //grf
    output reg_wen_w_A,
    output [31:0] grf_wdata_w_A,
    output [4:0] grf_waddr_w_A,

    input [31:0] pc_m2_B,
    input [31:0] pc4_m2_B,
    input [31:0] pc8_m2_B,
    input [31:0] ir_m2_B,
    input [31:0] ao_m2_B,
    input [31:0] dm_rdata_m2_B,
    input [1:0] wd3_sel_m2_B,
    input reg_wen_m2_B,
    input [44:0] data_hazard_m2_B,
    input [4:0] grf_waddr_m2_B,
    input isir_m2_B,
    input [31:0] cp0_grf_m2_B,
    output [31:0] data_hazard_w_B,
    //grf
    output reg_wen_w_B,
    output [31:0] grf_wdata_w_B,
    output [4:0] grf_waddr_w_B
    );

    wire [31:0] pc_w_A;
    wire [31:0] pc4_w_A;
    wire [31:0] pc8_w_A;
    wire [31:0] ir_w_A;
    wire [31:0] ao_w_A;
    wire [31:0] dm_rdata_w_A;
    wire [1:0] wd3_sel_w_A;
    wire isir_w_A;
    wire [31:0] cp0_grf_w_A;

    REGS_FLOW MW_A(
        .clk(clk),
        .reset(reset),
        .stall(stall_w),
        .clr(clr_w),
        .pc_I(pc_m2_A),
        .pc4_I(pc4_m2_A),
        .pc8_I(pc8_m2_A),
        .ir_I(ir_m2_A),
        .ao_I(ao_m2_A),
        .dm_rdata_I(dm_rdata_m2_A),
        .wd3_sel_I(wd3_sel_m2_A),
        .reg_wen_I(reg_wen_m2_A),
        .data_hazard_I(data_hazard_m2_A),
        .grf_waddr_I(grf_waddr_m2_A),
        .isir_I(isir_m2_A),
        .cp0_grf_I(cp0_grf_m2_A),

        .pc_O(pc_w_A),
        .pc4_O(pc4_w_A),
        .pc8_O(pc8_w_A),
        .ir_O(ir_w_A),
        .ao_O(ao_w_A),
        .dm_rdata_O(dm_rdata_w_A),
        .wd3_sel_O(wd3_sel_w_A),
        .reg_wen_O(reg_wen_w_A),
        .data_hazard_O(data_hazard_w_A),
        .grf_waddr_O(grf_waddr_w_A),
        .isir_O(isir_w_A),
        .cp0_grf_O(cp0_grf_w_A)
    );

    MUX_NORMAL mux_normal_A(
        .dm_rdata_w(dm_rdata_A),
        .ao_w(ao_w_A),
        .pc8_w(pc8_w_A),
        .cp0_reg_w(cp0_grf_w_A),
        .wd3_sel_w(wd3_sel_w_A),
        .grf_wdata_w(grf_wdata_w_A)
    );

    wire [31:0] pc_w_B;
    wire [31:0] pc4_w_B;
    wire [31:0] pc8_w_B;
    wire [31:0] ir_w_B;
    wire [31:0] ao_w_B;
    wire [31:0] dm_rdata_w_B;
    wire [1:0] wd3_sel_w_B;
    wire isir_w_B;
    wire [31:0] cp0_grf_w_B;

    REGS_FLOW MW_B(
        .clk(clk),
        .reset(reset),
        .stall(stall_w),
        .clr(clr_w),
        .pc_I(pc_m2_B),
        .pc4_I(pc4_m2_B),
        .pc8_I(pc8_m2_B),
        .ir_I(ir_m2_B),
        .ao_I(ao_m2_B),
        .dm_rdata_I(dm_rdata_m2_B),
        .wd3_sel_I(wd3_sel_m2_B),
        .reg_wen_I(reg_wen_m2_B),
        .data_hazard_I(data_hazard_m2_B),
        .grf_waddr_I(grf_waddr_m2_B),
        .isir_I(isir_m2_B),
        .cp0_grf_I(cp0_grf_m2_B),

        .pc_O(pc_w_B),
        .pc4_O(pc4_w_B),
        .pc8_O(pc8_w_B),
        .ir_O(ir_w_B),
        .ao_O(ao_w_B),
        .dm_rdata_O(dm_rdata_w_B),
        .wd3_sel_O(wd3_sel_w_B),
        .reg_wen_O(reg_wen_w_B),
        .data_hazard_O(data_hazard_w_B),
        .grf_waddr_O(grf_waddr_w_B),
        .isir_O(isir_w_B),
        .cp0_grf_O(cp0_grf_w_B)
    );

    MUX_NORMAL mux_normal_B(
        .dm_rdata_w(dm_rdata_B),
        .ao_w(ao_w_B),
        .pc8_w(pc8_w_B),
        .cp0_reg_w(cp0_grf_w_B),
        .wd3_sel_w(wd3_sel_w_B),
        .grf_wdata_w(grf_wdata_w_B)
    );

endmodule
