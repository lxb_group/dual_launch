`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/05 19:14:45
// Design Name: 
// Module Name: src_npc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NPC(
    input eret,
    input sig_intreq,
    input sig_stall,
    input [31:0] src_nom_pc,
    input [31:0] src_pc4_d2,
    input [25:0] src_ir26_d2,
    input [31:0] src_rf_rs_e,
    input [31:0] src_pc_bp,
    input sig_bp_error,
    input [1:0] sig_npc_opt_d1, 
    input [31:0] src_pc_f2,
    input [31:0] src_epc,
    input sig_jr_e,
    output [31:0] src_npc,
    output jump_branch_d1,
    output jump_branch_e
    );

    wire [31:0] npc_00, npc_10, src_npc_branch;
    assign npc_00 = src_nom_pc;
    assign src_npc_branch = src_pc4_d2 + {{14{src_ir26_d2[15]}}, src_ir26_d2[15:0], 2'b00};
    assign npc_10 = {src_pc4_d2[31:28], src_ir26_d2, 2'b00};

    assign jump_branch_d1 = sig_npc_opt_d1 != 2'b00;
    assign jump_branch_e = sig_jr_e;

    assign src_npc = sig_intreq? 32'hBFC00380:
                     eret?   src_epc:
                     sig_stall? src_pc_f2:
                     sig_bp_error? src_pc_bp:
                     sig_jr_e?    src_rf_rs_e:
                     (sig_npc_opt_d1 == 2'b00)? npc_00:
                     (sig_npc_opt_d1 == 2'b01)? src_npc_branch:
                     (sig_npc_opt_d1 == 2'b10)? npc_10:
                                                npc_00;

endmodule
