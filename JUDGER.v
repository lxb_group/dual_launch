`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/07 13:21:52
// Design Name: 
// Module Name: JUDGER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module JUDGER(
    input [44:0] data_A,
    input [44:0] data_B,
    input sig_stall_d2,
    input sig_clr_d2,
    input FIFO_empty,
    input FIFO_single,
    input md_A,
    input md_B,
    input jb_B,
    input eret_B,
    input sl_A,         //store_load
    input sl_B,
    output ren_A,
    output ren_B
    );

    wire [4:0] RS_A, RT_A, Tuse1_A, Tuse2_A, grf1_A, grf2_A, grfchange_A, Tnew_A, WD3_A;
    wire [4:0] RS_B, RT_B, Tuse1_B, Tuse2_B, grf1_B, grf2_B, grfchange_B, Tnew_B, WD3_B;
    wire data_conflict, d2_block;
    
    assign {RS_A, RT_A, Tuse1_A, Tuse2_A, grf1_A, grf2_A, grfchange_A, Tnew_A, WD3_A} = data_A;
    assign {RS_B, RT_B, Tuse1_B, Tuse2_B, grf1_B, grf2_B, grfchange_B, Tnew_B, WD3_B} = data_B;

    assign data_conflict = ((grfchange_A) && (((grfchange_A == grf1_B) && (Tnew_A > Tuse1_B)) || ((grfchange_A == grf2_B) && (Tnew_A > Tuse2_B))));
    assign d2_block = sig_stall_d2 || sig_clr_d2;

    assign ren_A = !FIFO_empty && !d2_block;
    
    assign ren_B = ren_A && !FIFO_single && !(md_A && md_B) && !data_conflict && !jb_B && !eret_B && !(sl_A && sl_B);

endmodule
