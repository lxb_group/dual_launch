`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/09 14:10:33
// Design Name: 
// Module Name: MD_F2T1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MD_F2T1(
    input   sig_md_A,
    input   sig_md_B,
    input   md_op_A,
    input   md_op_B,
    input   opt1_A,
    input   opt2_A,
    input   opt1_B,
    input   opt2_B,
    output  md_op,
    output  opt1,
    output  opt2
    );

    always @(*) begin
        if (sig_md_A) begin
            md_op = md_op_A;            
            opt1 = opt1_A;
            opt2 = opt2_A;
        end 
        else begin
            md_op = md_op_B;            
            opt1 = opt1_B;
            opt2 = opt2_B;
        end
    end

endmodule
