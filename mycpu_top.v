`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/14 16:15:33
// Design Name: 
// Module Name: mycpu_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mycpu_top(
    input clk,
    input resetn,
    input [5:0] int,

    output inst_sram_en,
    output [3:0] inst_sram_wen,
    output [31:0] inst_sram_addr,
    output [31:0] inst_sram_wdata,
    input [31:0] inst_sram_rdata,

    output data_sram_en,
    output [3:0] data_sram_wen,
    output [31:0] data_sram_addr,
    output [31:0] data_sram_wdata,
    input [31:0] data_sram_rdata,

    output [31:0] debug_wb_pc,
    output [3:0] debug_wb_rf_wen,
    output [4:0] debug_wb_rf_wnum,
    output [31:0] debug_wb_rf_wdata
    );

    wire reset;
    assign reset = ~resetn;
    wire eret;
    wire intreq;
    wire stall_npc;
    wire stall_f2;
    wire stall_fifo;
    wire clr_f2;
    wire clr_fifo;
    wire [31:0] pc4_d2_A;
    wire [31:0] instr_d2_A;
    wire [31:0] mf_rs_e_A;
    wire [31:0] pc_bp_error;
    wire        bp_error;
    wire [1:0]  npc_opt_d1_A;
    wire [31:0] epc;
    wire        JR_e_A;
    wire [31:0] pc_f1;
    wire [31:0] pc_f2;

    //FIFO
    wire [31:0] ren_A;
    wire [31:0] ren_B;


    wire NPC_jump_branch_d1;
    wire NPC_jump_branch_e;

    wire [31:0] FIFO_instr_A;
    wire [31:0] FIFO_instr_B;
    wire [31:0] FIFO_pc_A;
    wire [31:0] FIFO_pc_B;
    wire FIFO_full;
    wire FIFO_empty;
    wire FIFO_single;
    wire isir_f2;

    F_STAGE f_stage(
        .clk(clk),
        .reset(reset),
        .eret(eret),
        .intreq(intreq),
        .stall_npc(stall_npc),
        .stall_f2(stall_f2),
        .stall_fifo(stall_fifo),
        .clr_f2(clr_f2),
        .clr_fifo(clr_fifo),
        //NPC
        .pc4_d2_A(pc4_d2_A),
        .instr_d2_A(instr_d2_A),
        .mf_rs_e_A(mf_rs_e_A),
        .pc_bp_error(pc_bp_error),
        .bp_error(bp_error),
        .npc_opt_d1_A(npc_opt_d1_A),
        .epc(epc),
        .JR_e_A(JR_e_A),
        .pc_f1(pc_f1),
        //FIFO
        .inst_sram_rdata(inst_sram_rdata),
        .ren_A(ren_A),
        .ren_B(ren_B),
        .pc_f2(pc_f2),
        .isir_f2(isir_f2),
        .NPC_jump_branch_d1(NPC_jump_branch_d1),
        .NPC_jump_branch_e(NPC_jump_branch_e),
        .FIFO_instr_A(FIFO_instr_A),
        .FIFO_instr_B(FIFO_instr_B),
        .FIFO_pc_A(FIFO_pc_A),
        .FIFO_pc_B(FIFO_pc_B),
        .FIFO_full(FIFO_full),
        .FIFO_empty(FIFO_empty),
        .FIFO_single(FIFO_single)
    );
    
    wire res_cmp_e_A;

    wire stall_bpu;
    wire clr_bpu;
    wire stall_d2;
    wire clr_d2;
    wire grf_wen_w_A;
    wire [4:0] grf_waddr_w_A;
    wire [31:0] grf_wdata_w_A;

    wire grf_wen_w_B;
    wire [4:0] grf_waddr_w_B;
    wire [31:0] grf_wdata_w_B;

    wire [31:0] grf_rdata1_d2_A;
    wire [31:0] grf_rdata2_d2_A;
    wire [31:0] grf_rdata1_d2_B;
    wire [31:0] grf_rdata2_d2_;

    wire [31:0] imm_ext_d2_A;
    wire [31:0] imm_ext_d2_B;

    wire [31:0] pc_d2_A;
    wire [31:0] pc4_d2_A;
    wire [31:0] pc8_d2_A;
    wire [31:0] instr_d2_A;
    wire alu_srcb_sel_d2_A;
    wire [4:0] alu_opt_d2_A;
    wire dm_ren_d2_A;
    wire dm_wen_d2_A;
    wire [1:0] wd3_sel_d2_A;
    wire reg_wen_d2_A;
    wire [44:0] data_hazard_d2_A;
    wire [4:0] grf_waddr_d2_A;
    wire [2:0] cmp_opt_d2_A;
    wire [1:0] s_sel_d2_A;
    wire [2:0] ld_ctr_d2_A;
    wire md_d2_A;
    wire [2:0] md_op_d2_A;
    wire start_d2_A;
    wire AO_SEL_d2_A;
    wire BJ_d2_A;
    wire eret_d2_A;
    wire break_d2_A;
    wire syscall_d2_A;
    wire cp0_ren_d2_A;
    wire cp0_wen_d2_A;
    wire ri_d2_A;
    wire overable_d2_A;
    wire isir_d2_A;
    wire JR_d2_A;

    wire [31:0] pc_d2_B;
    wire [31:0] pc4_d2_B;
    wire [31:0] pc8_d2_B;
    wire [31:0] instr_d2_B;
    wire alu_srcb_sel_d2_B;
    wire [4:0] alu_opt_d2_B;
    wire dm_ren_d2_B;
    wire dm_wen_d2_B;
    wire [1:0] wd3_sel_d2_B;
    wire reg_wen_d2_B;
    wire [44:0] data_hazard_d2_B;
    wire [4:0] grf_waddr_d2_B;
    wire [2:0] cmp_opt_d2_B;
    wire [1:0] s_sel_d2_B;
    wire [2:0] ld_ctr_d2_B;
    wire md_d2_B;
    wire [2:0] md_op_d2_B;
    wire start_d2_B;
    wire AO_SEL_d2_B;
    wire BJ_d2_B;
    wire eret_d2_B;
    wire break_d2_B;
    wire syscall_d2_B;
    wire cp0_ren_d2_B;
    wire cp0_wen_d2_B;
    wire ri_d2_B;
    wire overable_d2_B;
    wire isir_d2_B;
    wire JR_d2_B;

    D_STAGE d_stage(
        .clk(clk),
        .reset(reset),
        .stall_bpu(stall_bpu),
        .clr_bpu(clr_bpu),
        .stall_d2(stall_d2),
        .clr_d2(clr_d2),

        .FIFO_instr_A(FIFO_instr_A),
        .FIFO_instr_B(FIFO_instr_B),
        .FIFO_pc_A(FIFO_pc_A),
        .FIFO_pc_B(FIFO_pc_B),
        .FIFO_empty(FIFO_empty),
        .FIFO_single(FIFO_single),
        .res_cmp_e_A(res_cmp_e_A),

        .grf_wen_w_A(grf_wen_w_A),
        .grf_waddr_w_A(grf_waddr_w_A),
        .grf_wdata_w_A(grf_wdata_w_A),

        .grf_wen_w_B(grf_wen_w_B),
        .grf_waddr_w_B(grf_waddr_w_B),
        .grf_wdata_w_B(grf_wdata_w_B),

        .pc_bp_error(pc_bp_error),
        .bp_error(bp_error),
        .npc_opt_d1_A(npc_opt_d1_A),

        .ren_A(ren_A),
        .ren_B(ren_B),
    //grf
        .grf_rdata1_d2_A(grf_rdata1_d2_A),
        .grf_rdata2_d2_A(grf_rdata2_d2_A),
        .grf_rdata1_d2_B(grf_rdata1_d2_B),
        .grf_rdata2_d2_B(grf_rdata2_d2_B),
    //ext
        .imm_ext_d2_A(imm_ext_d2_A),
        .imm_ext_d2_B(imm_ext_d2_B),
    //reg_flow
        .pc_d2_A(pc_d2_A),
        .pc4_d2_A(pc4_d2_A),
        .pc8_d2_A(pc8_d2_A),
        .instr_d2_A(instr_d2_A),
        .alu_srcb_sel_d2_A(alu_srcb_sel_d2_A),
        .alu_opt_d2_A(alu_opt_d2_A),
        .dm_ren_d2_A(dm_ren_d2_A),
        .dm_wen_d2_A(dm_wen_d2_A),
        .wd3_sel_d2_A(wd3_sel_d2_A),
        .reg_wen_d2_A(reg_wen_d2_A),
        .data_hazard_d2_A(data_hazard_d2_A),
        .grf_waddr_d2_A(grf_waddr_d2_A),
        .cmp_opt_d2_A(cmp_opt_d2_A),
        .s_sel_d2_A(s_sel_d2_A),
        .ld_ctr_d2_A(ld_ctr_d2_A),
        .md_d2_A(md_d2_A),
        .md_op_d2_A(md_op_d2_A),
        .start_d2_A(start_d2_A),
        .AO_SEL_d2_A(AO_SEL_d2_A),
        .BJ_d2_A(BJ_d2_A),
        .eret_d2_A(eret_d2_A),
        .break_d2_A(break_d2_A),
        .syscall_d2_A(syscall_d2_A),
        .cp0_ren_d2_A(cp0_ren_d2_A),
        .cp0_wen_d2_A(cp0_wen_d2_A),
        .ri_d2_A(ri_d2_A),
        .overable_d2_A(overable_d2_A),
        .isir_d2_A(isir_d2_A),
        .JR_d2_A(JR_d2_A),

        .pc_d2_B(pc_d2_B),
        .pc4_d2_B(pc4_d2_B),
        .pc8_d2_B(pc8_d2_B),
        .instr_d2_B(instr_d2_B),
        .alu_srcb_sel_d2_B(alu_srcb_sel_d2_B),
        .alu_opt_d2_B(alu_opt_d2_B),
        .dm_ren_d2_B(dm_ren_d2_B),
        .dm_wen_d2_B(dm_wen_d2_B),
        .wd3_sel_d2_B(wd3_sel_d2_B),
        .reg_wen_d2_B(reg_wen_d2_B),
        .data_hazard_d2_B(data_hazard_d2_B),
        .grf_waddr_d2_B(grf_waddr_d2_B),
        .cmp_opt_d2_B(cmp_opt_d2_B),
        .s_sel_d2_B(s_sel_d2_B),
        .ld_ctr_d2_B(ld_ctr_d2_B),
        .md_d2_B(md_d2_B),
        .md_op_d2_B(md_op_d2_B),
        .start_d2_B(start_d2_B),
        .AO_SEL_d2_B(AO_SEL_d2_B),
        .BJ_d2_B(BJ_d2_B),
        .eret_d2_B(eret_d2_B),
        .break_d2_B(break_d2_B),
        .syscall_d2_B(syscall_d2_B),
        .cp0_ren_d2_B(cp0_ren_d2_B),
        .cp0_wen_d2_B(cp0_wen_d2_B),
        .ri_d2_B(ri_d2_B),
        .overable_d2_B(overable_d2_B),
        .isir_d2_B(isir_d2_B),
        .JR_d2_B(JR_d2_B)
    );

    wire stall_md;
    wire stall_e;
    wire clr_e;
    wire [31:0] mf_rt_e_A;
    wire [31:0] mf_rs_e_B;
    wire [31:0] mf_rt_e_B;
    wire [31:0] grf_rdata1_e_A;
    wire [31:0] grf_rdata2_e_A;
    wire [31:0] grf_rdata1_e_B;
    wire [31:0] grf_rdata2_e_B;
    wire [31:0] pc8_e_A;

    wire start_e_A;
    wire [31:0] pc_e_A;
    wire [31:0] pc4_e_A;
    wire [31:0] instr_e_A;
    wire [31:0] ao_e_A;
    wire dm_ren_e_A;
    wire dm_wen_e_A;
    wire [1:0] wd3_sel_e_A;
    wire reg_wen_e_A;
    wire [44:0] data_hazard_e_A;
    wire [4:0] grf_waddr_e_A;
    wire [1:0] s_sel_e_A;
    wire [2:0] ld_ctr_e_A;
    wire BJ_e_A;
    wire eret_e_A;
    wire break_e_A;
    wire syscall_e_A;
    wire cp0_ren_e_A;
    wire cp0_wen_e_A;
    wire ri_e_A;
    wire overable_e_A;
    wire sig_over_e_A;
    wire isir_e_A;

    wire start_e_B;
    wire [31:0] pc_e_B;
    wire [31:0] pc4_e_B;
    wire [31:0] pc8_e_B;
    wire [31:0] instr_e_B;
    wire [31:0] ao_e_B;
    wire dm_ren_e_B;
    wire dm_wen_e_B;
    wire [1:0] wd3_sel_e_B;
    wire reg_wen_e_B;
    wire [44:0] data_hazard_e_B;
    wire [4:0] grf_waddr_e_B;
    wire [1:0] s_sel_e_B;
    wire [2:0] ld_ctr_e_B;
    wire BJ_e_B;
    wire eret_e_B;
    wire break_e_B;
    wire syscall_e_B;
    wire cp0_ren_e_B;
    wire cp0_wen_e_B;
    wire ri_e_B;
    wire overable_e_B;
    wire sig_over_e_B;
    wire isir_e_B;
    wire JR_e_B;

    wire MD_busy_e;

    E_STAGE e_stage(
        .clk(clk),
        .reset(reset),
        .stall_md(stall_md),
        .stall_e(stall_e),
        .clr_e(clr_e),
    //grf
        .grf_rdata1_d2_A(grf_rdata1_d2_A),
        .grf_rdata2_d2_A(grf_rdata2_d2_A),
        .grf_rdata1_d2_B(grf_rdata1_d2_B),
        .grf_rdata2_d2_B(grf_rdata2_d2_B),
    //ext
        .imm_ext_d2_A(imm_ext_d2_A),
        .imm_ext_d2_B(imm_ext_d2_B),
    //reg_flow
    //stop in e
        .alu_srcb_sel_d2_A(alu_srcb_sel_d2_A),
        .alu_opt_d2_A(alu_opt_d2_A),
        .cmp_opt_d2_A(cmp_opt_d2_A),
        .md_d2_A(md_d2_A),
        .md_op_d2_A(md_op_d2_A),
        .start_d2_A(start_d2_A),
        .AO_SEL_d2_A(AO_SEL_d2_A),

        .pc_d2_A(pc_d2_A),
        .pc4_d2_A(pc4_d2_A),
        .pc8_d2_A(pc8_d2_A),
        .instr_d2_A(instr_d2_A),
        .dm_ren_d2_A(dm_ren_d2_A),
        .dm_wen_d2_A(dm_wen_d2_A),
        .wd3_sel_d2_A(wd3_sel_d2_A),
        .reg_wen_d2_A(reg_wen_d2_A),
        .data_hazard_d2_A(data_hazard_d2_A),
        .grf_waddr_d2_A(grf_waddr_d2_A),
        .s_sel_d2_A(s_sel_d2_A),
        .ld_ctr_d2_A(ld_ctr_d2_A),
        .BJ_d2_A(BJ_d2_A),
        .eret_d2_A(eret_d2_A),
        .break_d2_A(break_d2_A),
        .syscall_d2_A(syscall_d2_A),
        .cp0_ren_d2_A(cp0_ren_d2_A),
        .cp0_wen_d2_A(cp0_wen_d2_A),
        .ri_d2_A(ri_d2_A),
        .overable_d2_A(overable_d2_A),
        .isir_d2_A(isir_d2_A),
        .JR_d2_A(JR_d2_A),

    //stop in e
        .alu_srcb_sel_d2_B(alu_srcb_sel_d2_B),
        .alu_opt_d2_B(alu_opt_d2_B),
        .cmp_opt_d2_B(cmp_opt_d2_B),
        .md_d2_B(md_d2_B),
        .md_op_d2_B(md_op_d2_B),
        .start_d2_B(start_d2_B),
        .AO_SEL_d2_B(AO_SEL_d2_B),

        .pc_d2_B(pc_d2_B),
        .pc4_d2_B(pc4_d2_B),
        .pc8_d2_B(pc8_d2_B),
        .instr_d2_B(instr_d2_B),
        .dm_ren_d2_B(dm_ren_d2_B),
        .dm_wen_d2_B(dm_wen_d2_B),
        .wd3_sel_d2_B(wd3_sel_d2_B),
        .reg_wen_d2_B(reg_wen_d2_B),
        .data_hazard_d2_B(data_hazard_d2_B),
        .grf_waddr_d2_B(grf_waddr_d2_B),
        .s_sel_d2_B(s_sel_d2_B),
        .ld_ctr_d2_B(ld_ctr_d2_B),
        .BJ_d2_B(BJ_d2_B),
        .eret_d2_B(eret_d2_B),
        .break_d2_B(break_d2_B),
        .syscall_d2_B(syscall_d2_B),
        .cp0_ren_d2_B(cp0_ren_d2_B),
        .cp0_wen_d2_B(cp0_wen_d2_B),
        .ri_d2_B(ri_d2_B),
        .overable_d2_B(overable_d2_B),
        .isir_d2_B(isir_d2_B),
        .JR_d2_B(JR_d2_B),

    //mux_forward
        .mf_rs_e_A(mf_rs_e_A),
        .mf_rt_e_A(mf_rt_e_A),
        .mf_rs_e_B(mf_rs_e_B),
        .mf_rt_e_B(mf_rt_e_B),

        .grf_rdata1_e_A(grf_rdata1_e_A),
        .grf_rdata2_e_A(grf_rdata2_e_A),
        .grf_rdata1_e_B(grf_rdata1_e_B),
        .grf_rdata2_e_B(grf_rdata2_e_B),
        .pc8_e_A(pc8_e_A),

        .pc_e_A(pc_e_A),
        .pc4_e_A(pc4_e_A),
        .instr_e_A(instr_e_A),
        .ao_e_A(ao_e_A),
        .dm_ren_e_A(dm_ren_e_A),
        .dm_wen_e_A(dm_wen_e_A),
        .wd3_sel_e_A(wd3_sel_e_A),
        .reg_wen_e_A(reg_wen_e_A),
        .data_hazard_e_A(data_hazard_e_A),
        .grf_waddr_e_A(grf_waddr_e_A),
        .s_sel_e_A(s_sel_e_A),
        .ld_ctr_e_A(ld_ctr_e_A),
        .BJ_e_A(BJ_e_A),
        .eret_e_A(eret_e_A),
        .break_e_A(break_e_A),
        .syscall_e_A(syscall_e_A),
        .cp0_ren_e_A(cp0_ren_e_A),
        .cp0_wen_e_A(cp0_wen_e_A),
        .ri_e_A(ri_e_A),
        .overable_e_A(overable_e_A),
        .sig_over_e_A(sig_over_e_A),
        .isir_e_A(isir_e_A),
        .JR_e_A(JR_e_A),

        .pc_e_B(pc_e_B),
        .pc4_e_B(pc4_e_B),
        .pc8_e_B(pc8_e_B),
        .instr_e_B(instr_e_B),
        .ao_e_B(ao_e_B),
        .dm_ren_e_B(dm_ren_e_B),
        .dm_wen_e_B(dm_wen_e_B),
        .wd3_sel_e_B(wd3_sel_e_B),
        .reg_wen_e_B(reg_wen_e_B),
        .data_hazard_e_B(data_hazard_e_B),
        .grf_waddr_e_B(grf_waddr_e_B),
        .s_sel_e_B(s_sel_e_B),
        .ld_ctr_e_B(ld_ctr_e_B),
        .BJ_e_B(BJ_e_B),
        .eret_e_B(eret_e_B),
        .break_e_B(break_e_B),
        .syscall_e_B(syscall_e_B),
        .cp0_ren_e_B(cp0_ren_e_B),
        .cp0_wen_e_B(cp0_wen_e_B),
        .ri_e_B(ri_e_B),
        .overable_e_B(overable_e_B),
        .sig_over_e_B(sig_over_e_B),
        .isir_e_B(isir_e_B),
        .JR_e_B(JR_e_B),

        .res_cmp_e_A(res_cmp_e_A),
        .MD_busy_e(MD_busy_e)
    );


    wire [31:0] cp0_pc_A;
    wire [31:0] cp0_pc_B;
    wire isir_m1_A;
    wire isir_m1_B;

    wire [31:0] pc_m1_A;
    wire [31:0] pc_m1_B;

    assign cp0_pc_A = isir_m1_A?    pc_m1_A : cp0_pc_B;

    assign cp0_pc_B = isir_m1_B?    pc_m1_B:
                      isir_e_A?     pc_e_A:
                      isir_e_B?     pc_e_B:
                      isir_d2_A?    pc_d2_A:
                      isir_d2_B?    pc_d2_B:
                      !FIFO_empty?  FIFO_instr_A:
                      isir_f2?      pc_f2:
                                    pc_f1;

    wire except_AB;

    wire md_e_A;
    wire md_e_B;

    wire [31:0] mf_rt_m1_A;
    wire [31:0] mf_rt_m1_B;
    wire [31:0] rf_rt_m1_A;
    wire [31:0] pc8_m1_A;
    wire [31:0] ao_m1_A;
    wire [44:0] data_hazard_m1_A;
    wire [31:0] rf_rt_m1_B;
    wire [31:0] pc8_m1_B;
    wire [31:0] ao_m1_B;
    wire [44:0] data_hazard_m1_B;

    wire [31:0] pc_m2_A;
    wire [31:0] pc4_m2_A;
    wire [31:0] pc8_m2_A;
    wire [31:0] ir_m2_A;
    wire [31:0] ao_m2_A;
    wire [31:0] dm_rdata_A;
    wire [1:0] wd3_sel_m2_A;
    wire reg_wen_m2_A;
    wire [44:0] data_hazard_m2_A;
    wire [4:0] grf_waddr_m2_A;
    wire isir_m2_A;
    wire [31:0] cp0_grf_m2_A;

    wire [31:0] pc_m2_B;
    wire [31:0] pc4_m2_B;
    wire [31:0] pc8_m2_B;
    wire [31:0] ir_m2_B;
    wire [31:0] ao_m2_B;
    wire [31:0] dm_rdata_B;
    wire [1:0] wd3_sel_m2_B;
    wire reg_wen_m2_B;
    wire [44:0] data_hazard_m2_B;
    wire [4:0] grf_waddr_m2_B;
    wire isir_m2_B;
    wire [31:0] cp0_grf_m2_B;

    M_STAGE m_stage(
        .clk(clk),
        .reset(reset),
        .stall_m1(stall_m1),
        .clr_m1(clr_m1),
        .stall_m2(stall_m2),
        .clr_m2_A(clr_m2_A),
        .clr_m2_B(clr_m2_B),

        .mf_rt_e_A(mf_rt_e_A),
        .mf_rt_e_B(mf_rt_e_B),
        .ao_e_A(ao_e_A),
        .ao_e_B(ao_e_B),

        .pc_e_A(pc_e_A),
        .pc4_e_A(pc4_e_A),
        .pc8_e_A(pc8_e_A),
        .instr_e_A(instr_e_A),
        .ao_e_A(ao_e_A),
        .dm_ren_e_A(dm_ren_e_A),
        .dm_wen_e_A(dm_wen_e_A),
        .wd3_sel_e_A(wd3_sel_e_A),
        .reg_wen_e_A(reg_wen_e_A),
        .data_hazard_e_A(data_hazard_e_A),
        .grf_waddr_e_A(grf_waddr_e_A),
        .s_sel_e_A(s_sel_e_A),
        .ld_ctr_e_A(ld_ctr_e_A),
        .BJ_e_A(BJ_e_A),
        .eret_e_A(eret_e_A),
        .break_e_A(break_e_A),
        .syscall_e_A(syscall_e_A),
        .cp0_ren_e_A(cp0_ren_e_A),
        .cp0_wen_e_A(cp0_wen_e_A),
        .ri_e_A(ri_e_A),
        .overable_e_A(overable_e_A),
        .sig_over_e_A(sig_over_e_A),
        .isir_e_A(isir_e_A),
        .JR_e_A(JR_e_A),

        .pc_e_B(pc_e_B),
        .pc4_e_B(pc4_e_B),
        .pc8_e_B(pc8_e_B),
        .instr_e_B(instr_e_B),
        .ao_e_B(ao_e_B),
        .dm_ren_e_B(dm_ren_e_B),
        .dm_wen_e_B(dm_wen_e_B),
        .wd3_sel_e_B(wd3_sel_e_B),
        .reg_wen_e_B(reg_wen_e_B),
        .data_hazard_e_B(data_hazard_e_B),
        .grf_waddr_e_B(grf_waddr_e_B),
        .s_sel_e_B(s_sel_e_B),
        .ld_ctr_e_B(ld_ctr_e_B),
        .BJ_e_B(BJ_e_B),
        .eret_e_B(eret_e_B),
        .break_e_B(break_e_B),
        .syscall_e_B(syscall_e_B),
        .cp0_ren_e_B(cp0_ren_e_B),
        .cp0_wen_e_B(cp0_wen_e_B),
        .ri_e_B(ri_e_B),
        .overable_e_B(overable_e_B),
        .sig_over_e_B(sig_over_e_B),
        .isir_e_B(isir_e_B),
        .JR_e_B(JR_e_B),
    //cp0
        .cp0_pc_A(cp0_pc_A),
        .cp0_pc_B(cp0_pc_B),
        .hwint(int),
        .cp0_epc(epc),
        .sig_intreq(intreq),
        .except_AB(except_AB),
        .eret_m1_A(eret),
        .isir_m1_A(isir_m1_A),
        .isir_m1_B(isir_m1_B),
        .pc_m1_A(pc_m1_A),
        .pc_m1_B(pc_m1_B),
    //mux_forward
        .md_e_A(md_e_A),
        .md_e_B(md_e_B),
        .mf_rt_m1_A(mf_rt_m1_A),
        .mf_rt_m1_B(mf_rt_m1_B),
        .rf_rt_m1_A(rf_rt_m1_A),
        .pc8_m1_A(pc8_m1_A),
        .ao_m1_A(ao_m1_A),
        .data_hazard_m1_A(data_hazard_m1_A),
        .rf_rt_m1_B(rf_rt_m1_B),
        .pc8_m1_B(pc8_m1_B),
        .ao_m1_B(ao_m1_B),
        .data_hazard_m1_B(data_hazard_m1_B),
    //sram
        .data_sram_rdata(data_sram_rdata),
        .data_sram_en(data_sram_en),
        .data_sram_wen(data_sram_wen),
        .data_sram_addr(data_sram_addr),
        .data_sram_wdata(data_sram_wdata),
    //reg_flow
        .pc_m2_A(pc_m2_A),
        .pc4_m2_A(pc4_m2_A),
        .pc8_m2_A(pc8_m2_A),
        .ir_m2_A(ir_m2_A),
        .ao_m2_A(ao_m2_A),
        .dm_rdata_A(dm_rdata_A),
        .wd3_sel_m2_A(wd3_sel_m2_A),
        .reg_wen_m2_A(reg_wen_m2_A),
        .data_hazard_m2_A(data_hazard_m2_A),
        .grf_waddr_m2_A(grf_waddr_m2_A),
        .isir_m2_A(isir_m2_A),
        .cp0_grf_m2_A(cp0_grf_m2_A),

        .pc_m2_B(pc_m2_B),
        .pc4_m2_B(pc4_m2_B),
        .pc8_m2_B(pc8_m2_B),
        .ir_m2_B(ir_m2_B),
        .ao_m2_B(ao_m2_B),
        .dm_rdata_B(dm_rdata_B),
        .wd3_sel_m2_B(wd3_sel_m2_B),
        .reg_wen_m2_B(reg_wen_m2_B),
        .data_hazard_m2_B(data_hazard_m2_B),
        .grf_waddr_m2_B(grf_waddr_m2_B),
        .isir_m2_B(isir_m2_B),
        .cp0_grf_m2_B(cp0_grf_m2_B)
    );

    wire [44:0] data_hazard_w_A;
    wire [44:0] data_hazard_w_B;

    W_STAGE w_stage(
        .clk(clk),
        .reset(reset),
        .stall_w(stall_w),
        .clr_w(clr_w),

        .pc_m2_A(pc_m2_A),
        .pc4_m2_A(pc4_m2_A),
        .pc8_m2_A(pc8_m2_A),
        .ir_m2_A(ir_m2_A),
        .ao_m2_A(ao_m2_A),
        .dm_rdata_m2_A(data_sram_rdata),
        .wd3_sel_m2_A(wd3_sel_m2_A),
        .reg_wen_m2_A(reg_wen_m2_A),
        .data_hazard_m2_A(data_hazard_m2_A),
        .grf_waddr_m2_A(grf_waddr_m2_A),
        .isir_m2_A(isir_m2_A),
        .cp0_grf_m2_A(cp0_grf_m2_A),

        .data_hazard_w_A(data_hazard_w_A),
    //grf
        .reg_wen_w_A(grf_wen_w_A),
        .grf_wdata_w_A(grf_wdata_w_A),
        .grf_waddr_w_A(grf_waddr_w_A),

        .pc_m2_B(pc_m2_B),
        .pc4_m2_B(pc4_m2_B),
        .pc8_m2_B(pc8_m2_B),
        .ir_m2_B(ir_m2_B),
        .ao_m2_B(ao_m2_B),
        .dm_rdata_m2_B(data_sram_rdatB),
        .wd3_sel_m2_B(wd3_sel_m2_B),
        .reg_wen_m2_B(reg_wen_m2_B),
        .data_hazard_m2_B(data_hazard_m2_B),
        .grf_waddr_m2_B(grf_waddr_m2_B),
        .isir_m2_B(isir_m2_B),
        .cp0_grf_m2_B(cp0_grf_m2_B),

        .data_hazard_w_B(data_hazard_w_B),
    //grf
        .reg_wen_w_B(grf_wen_w_B),
        .grf_wdata_w_B(grf_wdata_w_B),
        .grf_waddr_w_B(grf_wdata_w_B)
    );

    wire [4:0] sig_rs_e_A;
    wire [4:0] sig_rt_e_A;
    wire [4:0] sig_rs_e_B;
    wire [4:0] sig_rt_e_B;
    wire [4:0] sig_rt_m1_A;
    wire [4:0] sig_rt_m1_B;

    wire hazard_stall;

    HAZARD hazard(
        .Data_E_A(data_hazard_e_A),
        .Data_M1_A(data_hazard_m1_A),
        .Data_M2_A(data_hazard_m2_A),
        .Data_W_A(data_hazard_w_A),
        .Data_E_B(data_hazard_e_A),
        .Data_M1_B(data_hazard_m1_A),
        .Data_M2_B(data_hazard_m2_A),
        .Data_W_B(data_hazard_w_A),

        .BUSY_E(MD_busy_e),
        .MD_E_A(md_e_A),
        .MD_E_B(md_e_B),

        .sig_rs_e_A(sig_rs_e_A),
        .sig_rt_e_A(sig_rt_e_A),
        .sig_rs_e_B(sig_rs_e_B),
        .sig_rt_e_B(sig_rt_e_B),
        .sig_rt_m1_A(sig_rt_m1_A),
        .sig_rt_m1_B(sig_rt_m1_B),
    
        .hazard_stall(hazard_stall)
    );

    MUX_FORWARD mux_forward(
    //which need forward
        .src_rs_e_A(grf_rdata1_e_A),
        .src_rs_e_B(grf_rdata1_e_B),
        .src_rt_e_A(grf_rdata2_e_A),
        .src_rt_e_B(grf_rdata2_e_B),
        .src_rt_m1_A(rf_rt_m1_A),
        .src_rt_m1_B(rf_rt_m1_B),

    ///pc8 from d
        .src_pc8_e_A(pc8_e_A),
        .src_pc8_m1_A(pc8_m1_A),
        .src_pc8_m2_A(pc8_m2_A),

    //ao from m1
        .src_ao_m1_A(ao_m1_A),
        .src_ao_m1_B(ao_m1_B),
        .src_ao_m2_A(ao_m2_A),
        .src_ao_m2_B(ao_m2_B),

    //wd3 from w
        .src_wd3_w_A(grf_wdata_w_A),
        .src_wd3_w_B(grf_wdata_w_B),

    //cp0_m2
        .src_cp0_m2_A(cp0_grf_m2_A),
        .src_cp0_m2_B(cp0_grf_m2_B),

        .sig_rs_e_A(sig_rs_e_A),
        .sig_rt_e_A(sig_rt_e_A),
        .sig_rs_e_B(sig_rs_e_B),
        .sig_rt_e_B(sig_rt_e_B),
        .sig_rt_m1_A(sig_rt_m1_A),
        .sig_rt_m1_B(sig_rt_m1_B),

        .mf_rs_e_A(mf_rs_e_A),
        .mf_rt_e_A(mf_rt_e_A),
        .mf_rs_e_B(mf_rs_e_B),
        .mf_rt_e_B(mf_rt_e_B),
        .mf_rt_m1_A(mf_rt_m1_A),
        .mf_rt_m1_B(mf_rt_m1_B)
    );

    STALL_UNIT staLL_unit(
        .hazard_stall(hazard_stall),
        .sig_intreq(intreq),
        .except_AB(except_AB),
        .eret(eret),
        .jump_branch_d1(NPC_jump_branch_d1),
        .jump_branch_e(NPC_jump_branch_e),
        .bp_error(bp_error),
        .fifo_full(FIFO_full),

        .sig_stall_npc(stall_npc),
        .sig_stall_fifo(stall_fifo),
        .sig_stall_bpu(stall_fifo),
        .sig_stall_md(stall_md),

        .sig_stall_f2(stall_f2),
        .sig_stall_d2(stall_d2),
        .sig_stall_e(stall_e),
        .sig_stall_m1(stall_m1),
        .sig_stall_m2(stall_m2),
        .sig_stall_w(stall_w),

        .sig_clr_fifo(clr_fifo),
        .sig_clr_bpu(clr_bpu),

        .sig_clr_f2(clr_f2),
        .sig_clr_d2(clr_d2),
        .sig_clr_e(clr_e),
        .sig_clr_m1(clr_m1),
        .sig_clr_m2_A(clr_m2_A),
        .sig_clr_m2_B(clr_m2_B),
        .sig_clr_w(clr_w)
    );

endmodule
