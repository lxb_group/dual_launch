module TB_FIFO(
	);

	reg clk;
	reg reset;
	reg clr;
	reg [31:0] src_pc;
	reg [31:0] src_instr;
	reg wen;
	reg ren_A;
	reg ren_B;

	wire [31:0] src_instr_A;
	wire [31:0] src_instr_B;
	wire [31:0] src_pc_A;
	wire [31:0] src_pc_B;
	wire sig_full;
	wire sig_empty;
	wire sig_single;

	FIFO FIFO_test(
		.clk(clk),
		.reset(reset),
		.clr(clr),
		.src_pc(src_pc),
		.src_instr(src_instr),
		.wen(wen),
		.ren_A(ren_A),
		.ren_B(ren_B),
		.src_instr_A(src_instr_A),
		.src_instr_B(src_instr_B),
		.src_pc_A(src_pc_A),
		.src_pc_B(src_pc_B),
		.sig_full(sig_full),
		.sig_empty(sig_empty),
		.sig_single(sig_single)
	);

	initial begin
		clk = 0;
		reset = 1;
		src_pc = 32'hBFC00000;
		src_instr = 32'h0;
		wen = 0;
		ren_A = 0;
		ren_B = 0;
		#20
		reset = 0;
	end

	always #5 begin
		clk = ~clk;
	end

	always #10 begin
		src_pc = src_pc + 4;
		src_instr = src_instr + 4;
		wen = !sig_full;
		ren_A = !sig_empty && src_pc[3];
		ren_B = !sig_single && ren_A && src_pc[4];
		// ren_A = !sig_empty && src_pc[3:2] != 2'b01;
		// ren_B = !sig_single && ren_A && src_pc[3:2] == 2'b00;
	end

endmodule
