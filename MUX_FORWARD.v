`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/08 09:53:06
// Design Name: 
// Module Name: MUX_FORWARD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MUX_FORWARD(
    //which need forward
    input [31:0] src_rs_e_A,
    input [31:0] src_rs_e_B,
    input [31:0] src_rt_e_A,
    input [31:0] src_rt_e_B,
    input [31:0] src_rt_m1_A,
    input [31:0] src_rt_m1_B,

    ///pc8 from d
    input [31:0] src_pc8_e_A,
    input [31:0] src_pc8_m1_A,
    input [31:0] src_pc8_m2_A,

    //ao from m1
    input [31:0] src_ao_m1_A,
    input [31:0] src_ao_m1_B,
    input [31:0] src_ao_m2_A,
    input [31:0] src_ao_m2_B,

    //wd3 from w
    input [31:0] src_wd3_w_A,
    input [31:0] src_wd3_w_B,

    //cp0_m2
    input [31:0] src_cp0_m2_A,
    input [31:0] src_cp0_m2_B,

    input [4:0] sig_rs_e_A,
    input [4:0] sig_rt_e_A,
    input [4:0] sig_rs_e_B,
    input [4:0] sig_rt_e_B,
    input [4:0] sig_rt_m1_A,
    input [4:0] sig_rt_m1_B,

    output reg [31:0] mf_rs_e_A,
    output reg [31:0] mf_rt_e_A,
    output reg [31:0] mf_rs_e_B,
    output reg [31:0] mf_rt_e_B,
    output reg [31:0] mf_rt_m1_A,
    output reg [31:0] mf_rt_m1_B
    );

    always @(*) begin
        case (sig_rs_e_A)
            5'd1: mf_rs_e_A = src_ao_m1_B;
            5'd2: mf_rs_e_A = src_ao_m1_A;
            5'd3: mf_rs_e_A = src_pc8_m1_A;
            5'd4: mf_rs_e_A = src_ao_m2_B;
            5'd5: mf_rs_e_A = src_ao_m2_A;
            5'd6: mf_rs_e_A = src_pc8_m2_A;
            5'd7: mf_rs_e_A = src_cp0_m2_B;
            5'd8: mf_rs_e_A = src_cp0_m2_A;
            5'd9: mf_rs_e_A = src_wd3_w_B;
            5'd10: mf_rs_e_A = src_wd3_w_A;
            default: mf_rs_e_A = src_rs_e_A;
        endcase
        case (sig_rs_e_B)
            5'd1: mf_rs_e_B = src_pc8_e_A;
            5'd2: mf_rs_e_B = src_ao_m1_B;
            5'd3: mf_rs_e_B = src_ao_m1_A;
            5'd4: mf_rs_e_B = src_pc8_m1_A;
            5'd5: mf_rs_e_B = src_ao_m2_B;
            5'd6: mf_rs_e_B = src_ao_m2_A;
            5'd7: mf_rs_e_B = src_pc8_m2_A;
            5'd8: mf_rs_e_B = src_cp0_m2_B;
            5'd9: mf_rs_e_B = src_cp0_m2_A;
            5'd10: mf_rs_e_B = src_wd3_w_B;
            5'd11: mf_rs_e_B = src_wd3_w_A;
            default: mf_rs_e_B = src_rs_e_B;
        endcase
        case (sig_rt_e_A)
            5'd1: mf_rt_e_A = src_ao_m1_B;
            5'd2: mf_rt_e_A = src_ao_m1_A;
            5'd3: mf_rt_e_A = src_pc8_m1_A;
            5'd4: mf_rt_e_A = src_ao_m2_B;
            5'd5: mf_rt_e_A = src_ao_m2_A;
            5'd6: mf_rt_e_A = src_pc8_m2_A;
            5'd7: mf_rt_e_A = src_cp0_m2_B;
            5'd8: mf_rt_e_A = src_cp0_m2_A;
            5'd9: mf_rt_e_A = src_wd3_w_B;
            5'd10: mf_rt_e_A = src_wd3_w_A;
            default: mf_rt_e_A = src_rs_e_A;
        endcase
        case (sig_rt_e_B)
            5'd1: mf_rt_e_B = src_pc8_e_A;
            5'd2: mf_rt_e_B = src_ao_m1_B;
            5'd3: mf_rt_e_B = src_ao_m1_A;
            5'd4: mf_rt_e_B = src_pc8_m1_A;
            5'd5: mf_rt_e_B = src_ao_m2_B;
            5'd6: mf_rt_e_B = src_ao_m2_A;
            5'd7: mf_rt_e_B = src_pc8_m2_A;
            5'd8: mf_rt_e_B = src_cp0_m2_B;
            5'd9: mf_rt_e_B = src_cp0_m2_A;
            5'd10: mf_rs_e_B = src_wd3_w_B;
            5'd11: mf_rs_e_B = src_wd3_w_A;
            default: mf_rt_e_B = src_rs_e_B;
        endcase
        case (sig_rt_m1_A)
            5'd1: mf_rt_m1_A = src_ao_m2_B;
            5'd2: mf_rt_m1_A = src_ao_m2_A;
            5'd3: mf_rt_m1_A = src_pc8_m2_A;
            5'd4: mf_rt_m1_A = src_cp0_m2_B;
            5'd5: mf_rt_m1_A = src_cp0_m2_A;
            5'd6: mf_rt_m1_A = src_wd3_w_B;
            5'd7: mf_rt_m1_A = src_wd3_w_A;
            default: mf_rt_m1_A = src_rt_m1_A;
        endcase
        case (sig_rt_m1_B)
            5'd1: mf_rt_m1_B = src_ao_m1_A;
            5'd2: mf_rt_m1_B = src_pc8_m1_A;
            5'd3: mf_rt_m1_B = src_ao_m2_B;
            5'd4: mf_rt_m1_B = src_ao_m2_A;
            5'd5: mf_rt_m1_B = src_pc8_m2_A;
            5'd6: mf_rt_m1_B = src_cp0_m2_B;
            5'd7: mf_rt_m1_B = src_cp0_m2_A;
            5'd8: mf_rt_m1_B = src_wd3_w_B;
            5'd9: mf_rt_m1_B = src_wd3_w_A;
            default: mf_rt_m1_B = src_rt_m1_B;
        endcase
    end


endmodule
