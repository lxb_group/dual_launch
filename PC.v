`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/05 19:10:20
// Design Name: 
// Module Name: src_nom_pc
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PC(
    input clk,
    input reset,
    input [31:0] src_pc4_f1,
    output reg [31:0] src_nom_pc
    );

    always @(posedge clk)
        if(reset)
            src_nom_pc <= 32'hBFC0_0000;
        else begin
            src_nom_pc <= src_pc4_f1;
        end

endmodule
