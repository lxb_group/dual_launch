`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/07 19:11:23
// Design Name: 
// Module Name: CP0_F2T1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CP0_F2T1(
    input [31:0] src_pc_A,
    input [31:0] src_addr_A,
    input eret_A,                 //from ctr
    input sig_BD_A,               //from ctr 是否为延迟槽
    input sig_except_A,
    input [4:0] exccode_A,

    input [31:0] src_pc_B,
    input [31:0] src_addr_B,
    input eret_B,
    input sig_BD_B,
    input sig_except_B,
    input [4:0] exccode_B,

    output [31:0] src_pc,
    output [31:0] src_addr,
    output eret,
    output sig_BD,
    output sig_except,
    output [4:0] exccode,
    output except_AB
    );

    assign eret = eret_A || eret_B;
    assign sig_except = sig_except_A || sig_except_B;

    always @(*) begin
        if (!sig_except_A && sig_except_B) begin
            src_pc = src_pc_B;
            src_addr = src_addr_B;
            sig_BD = sig_BD_B;
            except_AB = `STREAM_B;
            exccode = exccode_B;
        end
        else begin
            src_pc = src_pc_A;
            src_addr = src_addr_A;
            sig_BD = sig_BD_A;
            except_AB = `STREAM_A;
            exccode = exccode_A;
        end
    end


endmodule
