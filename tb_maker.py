#!/usr/bin/python3

import sys
import re


def make(file_name):
    ins = []
    outs = []
    ins_name = []
    outs_name = []
    with open(file_name) as file:
        for line in file.readlines():
            splits = line.split(None, 1)
            if len(splits) == 0:
                continue
            if splits[0] == 'module':
                module_name = ''.join(re.split(r'[^A-Za-z0-9]', splits[1]))
            elif splits[0] == 'input':
                if splits[1].endswith('\n'):
                    splits[1] = splits[1][:-1]
                if splits[1].endswith(','):
                    splits[1] = splits[1][:-1]
                ins.append('\treg ' + splits[1] + ';\n')
                ins_name.append((splits[1].split())[-1])
            elif splits[0] == 'output':
                if splits[1].endswith('\n'):
                    splits[1] = splits[1][:-1]
                if splits[1].endswith(','):
                    splits[1] = splits[1][:-1]
                outs.append('\twire ' + splits[1] + ';\n')
                outs_name.append((splits[1].split())[-1])
    with open('TB_' + module_name + '.v', 'w') as tb:
        tb.write('module ' + 'TB_' + module_name + '(\n\t);\n\n')
        tb.writelines(ins)
        tb.write('\n')
        tb.writelines(outs)
        tb.write('\n')
        tb.write('\t' + module_name + ' ' + module_name + '_test(\n')
        for s in ins_name:
            tb.write('\t\t' + '.' + s + '(' + s + '),\n')
        for i in range(0, len(outs_name)):
            s = outs_name[i]
            if i == len(outs_name) - 1:
                tb.write('\t\t' + '.' + s + '(' + s + ')\n')
            else:
                tb.write('\t\t' + '.' + s + '(' + s + '),\n')
        tb.write('\t);\n\n')
        tb.write('\tinitial begin\n')
        for s in ins_name:
            tb.write('\t\t' + s + ' = 0;\n')
        tb.write('\n\tend\n\n')
        tb.write('endmodule\n')


for i in range(1, len(sys.argv)):
    make(sys.argv[i])
