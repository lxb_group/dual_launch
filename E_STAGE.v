`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/13 10:45:06
// Design Name: 
// Module Name: E_STAGE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module E_STAGE(
    input clk,
    input reset,
    input stall_md,
    input stall_e,
    input clr_e,
    //grf
    input [31:0] grf_rdata1_d2_A,
    input [31:0] grf_rdata2_d2_A,
    input [31:0] grf_rdata1_d2_B,
    input [31:0] grf_rdata2_d2_B,
    //ext
    input [31:0] imm_ext_d2_A,
    input [31:0] imm_ext_d2_B,
    //reg_flow
    //stop in e
    input alu_srcb_sel_d2_A,
    input [4:0] alu_opt_d2_A,
    input [2:0] cmp_opt_d2_A,
    input md_d2_A,
    input [2:0] md_op_d2_A,
    input start_d2_A,
    input AO_SEL_d2_A,

    input [31:0] pc_d2_A,
    input [31:0] pc4_d2_A,
    input [31:0] pc8_d2_A,
    input [31:0] instr_d2_A,
    input dm_ren_d2_A,
    input dm_wen_d2_A,
    input [1:0] wd3_sel_d2_A,
    input reg_wen_d2_A,
    input [44:0] data_hazard_d2_A,
    input [4:0] grf_waddr_d2_A,
    input [1:0] s_sel_d2_A,
    input [2:0] ld_ctr_d2_A,
    input BJ_d2_A,
    input eret_d2_A,
    input break_d2_A,
    input syscall_d2_A,
    input cp0_ren_d2_A,
    input cp0_wen_d2_A,
    input ri_d2_A,
    input overable_d2_A,
    input isir_d2_A,
    input JR_d2_A,

    //stop in e
    input alu_srcb_sel_d2_B,
    input [4:0] alu_opt_d2_B,
    input [2:0] cmp_opt_d2_B,
    input md_d2_B,
    input [2:0] md_op_d2_B,
    input start_d2_B,
    input AO_SEL_d2_B,

    input [31:0] pc_d2_B,
    input [31:0] pc4_d2_B,
    input [31:0] pc8_d2_B,
    input [31:0] instr_d2_B,
    input dm_ren_d2_B,
    input dm_wen_d2_B,
    input [1:0] wd3_sel_d2_B,
    input reg_wen_d2_B,
    input [44:0] data_hazard_d2_B,
    input [4:0] grf_waddr_d2_B,
    input [1:0] s_sel_d2_B,
    input [2:0] ld_ctr_d2_B,
    input BJ_d2_B,
    input eret_d2_B,
    input break_d2_B,
    input syscall_d2_B,
    input cp0_ren_d2_B,
    input cp0_wen_d2_B,
    input ri_d2_B,
    input overable_d2_B,
    input isir_d2_B,
    input JR_d2_B,

    //mux_forward
    output md_e_A,
    output md_e_B,
    input [31:0] mf_rs_e_A,
    input [31:0] mf_rt_e_A,
    input [31:0] mf_rs_e_B,
    input [31:0] mf_rt_e_B,
    output [31:0] grf_rdata1_e_A,
    output [31:0] grf_rdata2_e_A,
    output [31:0] grf_rdata1_e_B,
    output [31:0] grf_rdata2_e_B,
    output [31:0] pc8_e_A,

    output [31:0] pc_e_A,
    output [31:0] pc4_e_A,
    output [31:0] instr_e_A,
    output [31:0] ao_e_A,
    output dm_ren_e_A,
    output dm_wen_e_A,
    output [1:0] wd3_sel_e_A,
    output reg_wen_e_A,
    output [44:0] data_hazard_e_A,
    output [4:0] grf_waddr_e_A,
    output [1:0] s_sel_e_A,
    output [2:0] ld_ctr_e_A,
    output BJ_e_A,
    output eret_e_A,
    output break_e_A,
    output syscall_e_A,
    output cp0_ren_e_A,
    output cp0_wen_e_A,
    output ri_e_A,
    output overable_e_A,
    output sig_over_e_A,
    output isir_e_A,
    output JR_e_A,

    output [31:0] pc_e_B,
    output [31:0] pc4_e_B,
    output [31:0] pc8_e_B,
    output [31:0] instr_e_B,
    output [31:0] ao_e_B,
    output dm_ren_e_B,
    output dm_wen_e_B,
    output [1:0] wd3_sel_e_B,
    output reg_wen_e_B,
    output [44:0] data_hazard_e_B,
    output [4:0] grf_waddr_e_B,
    output [1:0] s_sel_e_B,
    output [2:0] ld_ctr_e_B,
    output BJ_e_B,
    output eret_e_B,
    output break_e_B,
    output syscall_e_B,
    output cp0_ren_e_B,
    output cp0_wen_e_B,
    output ri_e_B,
    output overable_e_B,
    output sig_over_e_B,
    output isir_e_B,
    output JR_e_B,

    output res_cmp_e_A,
    output MD_busy_e
    );

    wire start_e_A;
    wire start_e_B;
    
    wire alu_srcb_sel_e_A;
    wire [4:0] alu_opt_e_A;
    wire [2:0] cmp_opt_e_A;
    wire [2:0] md_op_e_A;
    wire ao_sel_e_A;
    wire alu_srcb_sel_e_B;
    wire [4:0] alu_opt_e_B;
    wire [2:0] cmp_opt_e_B;
    wire [2:0] md_op_e_B;
    wire AO_SEL_e_B;

    wire [31:0] alub_e_A;
    wire [31:0] alu_res_e_A;
    wire [31:0] hilo_e;
    wire sig_over_e_A;
    MUX_NORMAL mux_e_A(
        .rf_rt_e(mf_rs_e_A),
        .imm_ext_e(imm_ext_e_A),
        .alub_sel_e(alu_srcb_sel_e_A),
        .alub_e(alub_e_A),

        .alu_res_e(alu_res_e_A),
        .hilo_e(hilo_e),
        .ao_sel_e(ao_sel_e_A),
        .ao_e(ao_e_A)
    );
    ALU alu_A(
        .sig_opt(alu_opt_e_A),
        .src_a(mf_rs_e_A),
        .src_b(alub_e_A),
        .src_shamt(instr_e_A[10:6]),
        .src_out(alu_res_e_A),
        .sig_over(sig_over_e_A)
    );

    wire [31:0] alub_e_B;
    wire [31:0] alu_res_e_B;
    wire sig_over_e_B;
    MUX_NORMAL mux_e_B(
        .rf_rt_e(mf_rs_e_B),
        .imm_ext_e(imm_ext_e_B),
        .alub_sel_e(alu_srcb_sel_e_B),
        .alub_e(alub_e_B),

        .alu_res_e(alu_res_e_B),
        .hilo_e(hilo_e),
        .ao_sel_e(ao_sel_e_B),
        .ao_e(ao_e_B)
    );

    ALU alu_B(
        .sig_opt(alu_opt_e_B),
        .src_a(mf_rs_e_B),
        .src_b(alub_e_B),
        .src_shamt(instr_e_B[10:6]),
        .src_out(alu_res_e_B),
        .sig_over(sig_over_e_B)
    );

    MD_F2T1 md_f2t1(
        .sig_md_A(md_e_A),
        .sig_md_B(md_e_B),
        .md_op_A(md_op_e_A),
        .md_op_B(md_op_e_B),
        .opt1_A(mf_rs_e_A),
        .opt2_A(mf_rt_e_A),
        .opt1_B(mf_rs_e_B),
        .opt2_B(mf_rs_e_B),
        .md_op(MD_op_e),
        .opt1(MD_opt1),
        .opt2(MD_opt2)
    );

    MD md(
        .clk(clk),
        .reset(reset),
        .clr(1'b0),
        .sig_stall(stall_md),
        .sig_start(start_e_A || start_e_B),
        .md_op(MD_op_e),
        .opt1(MD_opt1),
        .opt2(MD_opt2),
        .HI_LO(hilo_e),
        .BUSY(MD_busy_e)
    );

    CMP cmp_A(
        .src_data1(mf_rs_e_A),
        .src_data2(mf_rt_e_A),
        .sig_cmp_opt(cmp_opt_e_A),
        .sig_res_cmp(res_cmp_e_A)
    );

    wire [31:0] grf_rdata1_e_A;
    wire [31:0] grf_rdata2_e_A;
    wire [31:0] imm_ext_e_A;
    wire [31:0] grf_rdata1_e_B;
    wire [31:0] grf_rdata2_e_B;
    wire [31:0] imm_ext_e_B;



    REGS_FLOW DE_A(
        .clk(clk),
        .reset(reset),
        .stall(stall_d2),          
        .clr(clr_d2),
        .pc_I(pc_d2_A),
        .pc4_I(pc4_d2_A),
        .pc8_I(pc8_d2_A),
        .ir_I(instr_d2_A),
        .rf_rs_I(grf_rdata1_d2_A),
        .rf_rt_I(grf_rdata2_d2_A),
        .imm_ext_I(imm_ext_d2_A),
        .alub_sel_I(alu_srcb_sel_d2_A),
        .alu_ctr_I(alu_opt_d2_A),
        .dm_ren_I(dm_ren_d2_A),
        .dm_wen_I(dm_wen_d2_A),
        .wd3_sel_I(wd3_sel_d2_A),
        .reg_wen_I(reg_wen_d2_A),
        .data_hazard_I(data_hazard_d2_A),
        .grf_waddr_I(grf_waddr_d2_A),
        .cmp_ctr_I(cmp_opt_d2_A),
        .s_sel_I(s_sel_d2_A),
        .ld_ctr_I(ld_ctr_d2_A),
        .md_I(md_d2_A),
        .md_op_I(md_op_d2_A),
        .start_I(start_d2_A),
        .ao_sel_I(AO_SEL_d2_A),
        .bj_I(BJ_d2_A),
        .eret_I(eret_d2_A),
        .break_I(break_d2_A),
        .syscall_I(syscall_d2_A),
        .cp0_ren_I(cp0_ren_d2_A),
        .cp0_wen_I(cp0_wen_d2_A),
        .ri_I(ri_d2_A),
        .overable_I(overable_d2_A),
        .isir_I(ren_A),
        .jr_I(JR_d2_A),

        .pc_O(pc_e_A),
        .pc4_O(pc4_e_A),
        .pc8_O(pc8_e_A),
        .ir_O(instr_e_A),
        .rf_rs_O(grf_rdata1_e_A),
        .rf_rt_O(grf_rdata2_e_A),
        .imm_ext_O(imm_ext_e_A),
        .alub_sel_O(alu_srcb_sel_e_A),
        .alu_ctr_O(alu_opt_e_A),
        .dm_ren_O(dm_ren_e_A),
        .dm_wen_O(dm_wen_e_A),
        .wd3_sel_O(wd3_sel_e_A),
        .reg_wen_O(reg_wen_e_A),
        .data_hazard_O(data_hazard_e_A),
        .grf_waddr_O(grf_waddr_e_A),
        .cmp_ctr_O(cmp_opt_e_A),
        .s_sel_O(s_sel_e_A),
        .ld_ctr_O(ld_ctr_e_A),
        .md_O(md_e_A),
        .md_op_O(md_op_e_A),
        .start_O(start_e_A),
        .ao_sel_O(ao_sel_e_A),
        .bj_O(BJ_e_A),
        .eret_O(eret_e_A),
        .break_O(break_e_A),
        .syscall_O(syscall_e_A),
        .cp0_ren_O(cp0_ren_e_A),
        .cp0_wen_O(cp0_wen_e_A),
        .ri_O(ri_e_A),
        .overable_O(overable_e_A),
        .isir_O(isir_e_A),
        .jr_O(JR_e_A)
    );

     REGS_FLOW DE_B(
        .clk(clk),
        .reset(reset),
        .stall(stall_d2),          
        .clr(clr_d2),
        .pc_I(pc_d2_B),
        .pc4_I(pc4_d2_B),
        .pc8_I(pc8_d2_B),
        .ir_I(instr_d2_B),
        .rf_rs_I(grf_rdata1_d2_B),
        .rf_rt_I(grf_rdata2_d2_B),
        .imm_ext_I(imm_ext_d2_B),
        .alub_sel_I(alu_srcb_sel_d2_B),
        .alu_ctr_I(alu_opt_d2_B),
        .dm_ren_I(dm_ren_d2_B),
        .dm_wen_I(dm_wen_d2_B),
        .wd3_sel_I(wd3_sel_d2_B),
        .reg_wen_I(reg_wen_d2_B),
        .data_hazard_I(data_hazard_d2_B),
        .grf_waddr_I(grf_waddr_d2_B),
        .cmp_ctr_I(cmp_opt_d2_B),
        .s_sel_I(s_sel_d2_B),
        .ld_ctr_I(ld_ctr_d2_B),
        .md_I(md_d2_B),
        .md_op_I(md_op_d2_B),
        .start_I(start_d2_B),
        .ao_sel_I(AO_SEL_d2_B),
        .bj_I(BJ_d2_B),
        .eret_I(eret_d2_B),
        .break_I(break_d2_B),
        .syscall_I(syscall_d2_B),
        .cp0_ren_I(cp0_ren_d2_B),
        .cp0_wen_I(cp0_wen_d2_B),
        .ri_I(ri_d2_B),
        .overable_I(overable_d2_B),
        .isir_I(ren_B),
        .jr_I(JR_d2_B),

        .pc_O(pc_e_B),
        .pc4_O(pc4_e_B),
        .pc8_O(pc8_e_B),
        .ir_O(instr_e_B),
        .rf_rs_O(grf_rdata1_e_B),
        .rf_rt_O(grf_rdata2_e_B),
        .imm_ext_O(imm_ext_e_B),
        .alub_sel_O(alu_srcb_sel_e_B),
        .alu_ctr_O(alu_opt_e_B),
        .dm_ren_O(dm_ren_e_B),
        .dm_wen_O(dm_wen_e_B),
        .wd3_sel_O(wd3_sel_e_B),
        .reg_wen_O(reg_wen_e_B),
        .data_hazard_O(data_hazard_e_B),
        .grf_waddr_O(grf_waddr_e_B),
        .cmp_ctr_O(cmp_opt_e_B),
        .s_sel_O(s_sel_e_B),
        .ld_ctr_O(ld_ctr_e_B),
        .md_O(md_e_B),
        .md_op_O(md_op_e_B),
        .start_O(start_e_B),
        .ao_sel_O(AO_SEL_e_B),
        .bj_O(BJ_e_B),
        .eret_O(eret_e_B),
        .break_O(break_e_B),
        .syscall_O(syscall_e_B),
        .cp0_ren_O(cp0_ren_e_B),
        .cp0_wen_O(cp0_wen_e_B),
        .ri_O(ri_e_B),
        .overable_O(overable_e_B),
        .isir_O(isir_e_B),
        .jr_O(JR_e_B)
    );

endmodule
