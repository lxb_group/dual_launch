`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/04 08:10:22
// Design Name: 
// Module Name: CP0
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CP0(
    input clk,
    input reset,
    input [31:0] src_pc_cp0,
    input [31:0] src_addr,
    input [5:0] hwint,     
    input eret,                         //from ctr
    input sig_BD,                       //from ctr 是否为延迟槽
    input sig_except,                   //from check
    input [4:0] src_exccode,            //from check
    input [4:0] cp0_addr_A,             //GPR[rt] ← CP0[rd, sel]
    input [4:0] cp0_addr_B,             //GPR[rt] ← CP0[rd, sel]
    input sig_wen_A,                    //from ctr
    input sig_wen_B,                    //from ctr
    input [31:0] src_wdata_A,             
    input [31:0] src_wdata_B, 
    input except_AB_exception,            
    output reg [31:0] src_rdata_A,        //mux with AO, AC_SEL
    output reg [31:0] src_rdata_B,        //mux with AO, AC_SEL
    output reg [31:0] src_epc,
    output sig_intreq,
    output except_AB
    );

    reg bd, ti, exl, ie;
    reg [4:0] exccode;
    reg [7:0] ip, im;
    reg [31:0] badvaddr;
    reg [32:0] count;

    wire shutdown;
    assign shutdown = (|(ip & im)) & ie;
    assign sig_intreq = (shutdown || sig_except) & !exl;
    assign except_AB = shutdown? `STREAM_A : except_AB_exception;

    always @(*) begin
        //asume that just in kernel model
        if (cp0_addr_B == cp0_addr_A && sig_wen_A) begin
            case (cp0_addr_B)
                `BADVADDR :src_rdata_B = badvaddr;
                `COUNT    :src_rdata_B = src_wdata_A;
                `STATUS   :src_rdata_B = {9'd0, 1'b1, 6'd0, src_wdata_A[15:8], 6'd0, src_wdata_A[1:0]};
                `CAUSE    :src_rdata_B = {bd, ti, 14'd0, ip[7:2], src_wdata_A[9:8], 1'd0, exccode, 2'd0};
                `EPC      :src_rdata_B = src_wdata_A;
                default   :src_rdata_B = 32'd88488848;
            endcase
        end
        else begin
            case (cp0_addr_B)
                `BADVADDR : src_rdata_B = badvaddr;
                `COUNT    : src_rdata_B = count[32:1];
                `STATUS   : src_rdata_B = {9'd0, 1'b1, 6'd0, im, 6'd0, exl, ie};
                `CAUSE    : src_rdata_B = {bd, ti, 14'd0, ip, 1'd0, exccode, 2'd0};
                `EPC      : src_rdata_B = src_epc;
                default   : src_rdata_B = 32'd88488848; 
            endcase
        end
        case (cp0_addr_A)
            `BADVADDR : src_rdata_A = badvaddr;
            `COUNT    : src_rdata_A = count[32:1];
            `STATUS   : src_rdata_A = {9'd0, 1'b1, 6'd0, im, 6'd0, exl, ie};
            `CAUSE    : src_rdata_A = {bd, ti, 14'd0, ip, 1'd0, exccode, 2'd0};
            `EPC      : src_rdata_A = src_epc;
            default   : src_rdata_A = 32'd88488848;
        endcase
    end

    always @(posedge clk) begin
        count = count + 1;
        if(reset) begin
            src_epc <= 0;
            bd <= 0;
            ti <= 0;
            exl <= 0;
            ie <= 0;
            exccode <= 0;
            ip <= 0;
            im <= 0;
            count <= 0;
            badvaddr <= 0;
        end
        else begin
            ip[7:2] <= hwint;
            if(sig_intreq) begin
                exl <= 1'b1;
                bd <= sig_BD;
                src_epc <= sig_BD? src_pc_cp0-4 : src_pc_cp0;
                if(shutdown)
                    exccode <= 5'd0;
                else begin
                    exccode <= src_exccode;
                    badvaddr <= src_addr;
                end
            end
            else if(eret) begin
                exl <= 1'b0;
                bd <= 1'b0;
            end
            else begin
                if (sig_wen_B) begin
                    case (cp0_addr_B)
                        `COUNT : count <= src_wdata_B;
                        `STATUS: begin
                            im <= src_wdata_B[15:8];
                            exl <= src_wdata_B[1];
                            ie <= src_wdata_B[0];
                        end
                        `CAUSE: ip[1:0] <= src_wdata_B[9:8];
                        `EPC  : src_epc[31:0] <= src_wdata_B[31:0];
                    endcase
                end
                if(sig_wen_A && !(sig_wen_B && cp0_addr_A == cp0_addr_B)) begin
                    case (cp0_addr_A)
                        `COUNT : count[32:0] <= {src_wdata_A, 1'b0};
                        `STATUS: begin
                            im <= src_wdata_A[15:8];
                            exl <= src_wdata_A[1];
                            ie <= src_wdata_A[0];
                        end
                        `CAUSE: ip[1:0] <= src_wdata_A[9:8];
                        `EPC  : src_epc[31:0] <= src_wdata_A[31:0];
                    endcase
                end
            end
        end
    end

endmodule

