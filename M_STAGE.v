`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/13 17:07:57
// Design Name: 
// Module Name: M_STAGE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module M_STAGE(
    input clk,
    input reset,
    input stall_m1,
    input clr_m1,
    input stall_m2,
    input clr_m2_A,
    input clr_m2_B,

    input [31:0] mf_rt_e_A,
    input [31:0] mf_rt_e_B,
    input [31:0] ao_e_A,
    input [31:0] ao_e_B,

    input [31:0] pc_e_A,
    input [31:0] pc4_e_A,
    input [31:0] pc8_e_A,
    input [31:0] instr_e_A,
    input dm_ren_e_A,
    input dm_wen_e_A,
    input [1:0] wd3_sel_e_A,
    input reg_wen_e_A,
    input [44:0] data_hazard_e_A,
    input [4:0] grf_waddr_e_A,
    input [1:0] s_sel_e_A,
    input [2:0] ld_ctr_e_A,
    input BJ_e_A,
    input eret_e_A,
    input break_e_A,
    input syscall_e_A,
    input cp0_ren_e_A,
    input cp0_wen_e_A,
    input ri_e_A,
    input overable_e_A,
    input sig_over_e_A,
    input isir_e_A,
    input JR_e_A,

    input [31:0] pc_e_B,
    input [31:0] pc4_e_B,
    input [31:0] pc8_e_B,
    input [31:0] instr_e_B,
    input dm_ren_e_B,
    input dm_wen_e_B,
    input [1:0] wd3_sel_e_B,
    input reg_wen_e_B,
    input [44:0] data_hazard_e_B,
    input [4:0] grf_waddr_e_B,
    input [1:0] s_sel_e_B,
    input [2:0] ld_ctr_e_B,
    input BJ_e_B,
    input eret_e_B,
    input break_e_B,
    input syscall_e_B,
    input cp0_ren_e_B,
    input cp0_wen_e_B,
    input ri_e_B,
    input overable_e_B,
    input sig_over_e_B,
    input isir_e_B,
    input JR_e_B,
    //cp0
    input [31:0] cp0_pc_A,
    input [31:0] cp0_pc_B,
    input [5:0] hwint,
    output [31:0] cp0_epc,
    output sig_intreq,
    output except_AB,
    output eret_m1_A,
    output isir_m1_A,
    output isir_m1_B,
    output [31:0] pc_m1_A,
    output [31:0] pc_m1_B,
    //mux_forward
    input [31:0] mf_rt_m1_A,
    input [31:0] mf_rt_m1_B,
    output [31:0] rf_rt_m1_A,
    output [31:0] pc8_m1_A,
    output [31:0] ao_m1_A,
    output [44:0] data_hazard_m1_A,
    output [31:0] rf_rt_m1_B,
    output [31:0] pc8_m1_B,
    output [31:0] ao_m1_B,
    output [44:0] data_hazard_m1_B,
    //sram
    input [31:0] data_sram_rdata,
    output data_sram_en,
    output [3:0] data_sram_wen,
    output [31:0] data_sram_addr,
    output [31:0] data_sram_wdata,
    //reg_flow
    output [31:0] pc_m2_A,
    output [31:0] pc4_m2_A,
    output [31:0] pc8_m2_A,
    output [31:0] ir_m2_A,
    output [31:0] ao_m2_A,
    output [31:0] dm_rdata_A,
    output [1:0] wd3_sel_m2_A,
    output reg_wen_m2_A,
    output [44:0] data_hazard_m2_A,
    output [4:0] grf_waddr_m2_A,
    output isir_m2_A,
    output [31:0] cp0_grf_m2_A,

    output [31:0] pc_m2_B,
    output [31:0] pc4_m2_B,
    output [31:0] pc8_m2_B,
    output [31:0] ir_m2_B,
    output [31:0] ao_m2_B,
    output [31:0] dm_rdata_B,
    output [1:0] wd3_sel_m2_B,
    output reg_wen_m2_B,
    output [44:0] data_hazard_m2_B,
    output [4:0] grf_waddr_m2_B,
    output isir_m2_B,
    output [31:0] cp0_grf_m2_B
    );

    wire [31:0] pc4_m1_A;
    wire [31:0] instr_m1_A;
    wire dm_ren_m1_A;
    wire dm_wen_m1_A;
    wire [1:0] wd3_sel_m1_A;
    wire reg_wen_m1_A;
    wire [4:0] grf_waddr_m1_A;
    wire [1:0] s_sel_m1_A;
    wire [2:0] ld_ctr_m1_A;
    wire BJ_m1_A;
    wire eret_m1_A;
    wire break_m1_A;
    wire syscall_m1_A;
    wire cp0_ren_m1_A;
    wire cp0_wen_m1_A;
    wire ri_m1_A;
    wire overable_m1_A;
    wire sig_over_m1_A;
    wire JR_m1_A;

    REGS_FLOW EM_A(
        .clk(clk),
        .reset(reset),
        .stall(stall_m1),
        .clr(clr_m1),
        .pc_I(pc_e_A),
        .pc4_I(pc4_e_A),
        .pc8_I(pc8_e_A),
        .ir_I(instr_e_A),
        .rf_rt_I(mf_rt_e_A),
        .ao_I(ao_e_A),
        .dm_ren_I(dm_ren_e_A),
        .dm_wen_I(dm_wen_e_A),
        .wd3_sel_I(wd3_sel_e_A),
        .reg_wen_I(reg_wen_e_A),
        .data_hazard_I(data_hazard_e_A),
        .grf_waddr_I(grf_waddr_e_A),
        .s_sel_I(s_sel_e_A),
        .ld_ctr_I(ld_ctr_e_A),
        .bj_I(BJ_e_A),
        .eret_I(eret_e_A),
        .break_I(break_e_A),
        .syscall_I(syscall_e_A),
        .cp0_ren_I(cp0_ren_e_A),
        .cp0_wen_I(cp0_wen_e_A),
        .ri_I(ri_e_A),
        .overable_I(overable_e_A),
        .over_I(sig_over_e_A),
        .isir_I(isir_e_A),
        .jr_I(JR_e_A),

        .pc_O(pc_m1_A),
        .pc4_O(pc4_m1_A),
        .pc8_O(pc8_m1_A),
        .ir_O(instr_m1_A),
        .rf_rt_O(rf_rt_m1_A),
        .ao_O(ao_m1_A),
        .dm_ren_O(dm_ren_m1_A),
        .dm_wen_O(dm_wen_m1_A),
        .wd3_sel_O(wd3_sel_m1_A),
        .reg_wen_O(reg_wen_m1_A),
        .data_hazard_O(data_hazard_m1_A),
        .grf_waddr_O(grf_waddr_m1_A),
        .s_sel_O(s_sel_m1_A),
        .ld_ctr_O(ld_ctr_m1_A),
        .bj_O(BJ_m1_A),
        .eret_O(eret_m1_A),
        .break_O(break_m1_A),
        .syscall_O(syscall_m1_A),
        .cp0_ren_O(cp0_ren_m1_A),
        .cp0_wen_O(cp0_wen_m1_A),
        .ri_O(ri_m1_A),
        .overable_O(overable_m1_A),
        .over_O(sig_over_m1_A),
        .isir_O(isir_m1_A),
        .jr_O(JR_m1_A)
    );

    wire [31:0] pc4_m1_B;
    wire [31:0] instr_m1_B;
    wire dm_ren_m1_B;
    wire dm_wen_m1_B;
    wire [1:0] wd3_sel_m1_B;
    wire reg_wen_m1_B;
    wire [4:0] grf_waddr_m1_B;
    wire [1:0] s_sel_m1_B;
    wire [2:0] ld_ctr_m1_B;
    wire BJ_m1_B;
    wire eret_m1_B;
    wire break_m1_B;
    wire syscall_m1_B;
    wire cp0_ren_m1_B;
    wire cp0_wen_m1_B;
    wire ri_m1_B;
    wire overable_m1_B;
    wire sig_over_m1_B;
    wire JR_m1_B;

    REGS_FLOW EM_B(
        .clk(clk),
        .reset(reset),
        .stall(stall_m1),
        .clr(clr_m1),
        .pc_I(pc_e_B),
        .pc4_I(pc4_e_B),
        .pc8_I(pc8_e_B),
        .ir_I(instr_e_B),
        .rf_rt_I(mf_rt_e_B),
        .ao_I(ao_e_B),
        .dm_ren_I(dm_ren_e_B),
        .dm_wen_I(dm_wen_e_B),
        .wd3_sel_I(wd3_sel_e_B),
        .reg_wen_I(reg_wen_e_B),
        .data_hazard_I(data_hazard_e_B),
        .grf_waddr_I(grf_waddr_e_B),
        .s_sel_I(s_sel_e_B),
        .ld_ctr_I(ld_ctr_e_B),
        .bj_I(BJ_e_B),
        .eret_I(eret_e_B),
        .break_I(break_e_B),
        .syscall_I(syscall_e_B),
        .cp0_ren_I(cp0_ren_e_B),
        .cp0_wen_I(cp0_wen_e_B),
        .ri_I(ri_e_B),
        .overable_I(overable_e_B),
        .over_I(sig_over_e_B),
        .isir_I(isir_e_B),
        .jr_I(JR_e_B),

        .pc_O(pc_m1_B),
        .pc4_O(pc4_m1_B),
        .pc8_O(pc8_m1_B),
        .ir_O(instr_m1_B),
        .rf_rt_O(mf_rt_m1_B),
        .ao_O(ao_m1_B),
        .dm_ren_O(dm_ren_m1_B),
        .dm_wen_O(dm_wen_m1_B),
        .wd3_sel_O(wd3_sel_m1_B),
        .reg_wen_O(reg_wen_m1_B),
        .data_hazard_O(data_hazard_m1_B),
        .grf_waddr_O(grf_waddr_m1_B),
        .s_sel_O(s_sel_m1_B),
        .ld_ctr_O(ld_ctr_m1_B),
        .bj_O(BJ_m1_B),
        .eret_O(eret_m1_B),
        .break_O(break_m1_B),
        .syscall_O(syscall_m1_B),
        .cp0_ren_O(cp0_ren_m1_B),
        .cp0_wen_O(cp0_wen_m1_B),
        .ri_O(ri_m1_B),
        .overable_O(overable_m1_B),
        .over_O(sig_over_m1_B),
        .isir_O(isir_m1_B),
        .jr_O(JR_m1_B)
    );

    wire sig_exc_A;
    wire [4:0] exccode_A;
    wire [31:0] badaddr_A;

    wire sig_exc_B;
    wire [4:0] exccode_B;
    wire [31:0] badaddr_B;

    CHECK check_A(
        .src_pc_m(pc_m1_A),
        .src_s_sel_m(s_sel_m1_A),
        .src_sl_addr(ao_m1_A),
        .sig_dm_ren_m(dm_ren_m1_A),
        .sig_dm_wen_m(dm_wen_m1_A),
        .sig_ri(ri_m1_A),
        .break(break_m1_A),
        .syscall(syscall_m1_A),
        .sig_overable(overable_m1_A),
        .over(sig_over_m1_A),
        .sig_exc(sig_exc_A),
        .src_exccode(exccode_A),
        .src_badaddr(badaddr_A)
    );

    CHECK check_B(
        .src_pc_m(pc_m1_B),
        .src_s_sel_m(s_sel_m1_B),
        .src_sl_addr(ao_m1_B),
        .sig_dm_ren_m(dm_ren_m1_B),
        .sig_dm_wen_m(dm_wen_m1_B),
        .sig_ri(ri_m1_B),
        .break(break_m1_B),
        .syscall(syscall_m1_B),
        .sig_overable(overable_m1_B),
        .over(sig_over_m1_B),
        .sig_exc(sig_exc_B),
        .src_exccode(exccode_B),
        .src_badaddr(badaddr_B)
    );

    reg bd_m1_A;
    wire bd_m1_B;
    
    always @(posedge clk) begin
        if (reset) begin
            bd_m1_A <= 1'b0;
        end
        else if (isir_m1_B) begin
            bd_m1_A <= BJ_m1_B;
        end
        else if (isir_m1_A) begin
            bd_m1_A <= BJ_m1_A;
        end
    end

    assign bd_m1_B = (isir_m1_A)? BJ_m1_A : bd_m1_A;
    wire [31:0] CP0_pc;
    wire [31:0] CP0_badaddr;
    wire CP0_eret;
    wire CP0_sig_BD;
    wire CP0_sig_except;
    wire [4:0] CP0_exccode;
    wire CP0_except_AB;

    CP0_F2T1 cp0_f2t1(
    .src_pc_A(cp0_pc_A),
    .src_addr_A(badaddr_A),
    .eret_A(eret_m1_A),                 //from ctr
    .sig_BD_A(bd_m1_A),               //from ctr 是否为延迟槽
    .sig_except_A(sig_exc_A),
    .exccode_A(exccode_A),

    .src_pc_B(cp0_pc_B),
    .src_addr_B(badaddr_B),
    .eret_B(eret_m1_B),
    .sig_BD_B(bd_m1_B),
    .sig_except_B(sig_exc_B),
    .exccode_B(exccode_B),

    .src_pc(CP0_pc),
    .src_addr(CP0_badaddr),
    .eret(CP0_eret),
    .sig_BD(CP0_sig_BD),
    .sig_except(CP0_sig_except),
    .exccode(CP0_exccode),
    .except_AB(CP0_except_AB)
    );
    
    wire [31:0] cp0_rdata_m1_A;
    wire [31:0] cp0_rdata_m1_A;

    CP0 cp0(
        .clk(clk),
        .reset(reset),
        .src_pc_cp0(CP0_pc),
        .src_addr(CP0_badaddr),
        .hwint(hwint),
        .eret(CP0_eret),
        .sig_BD(CP0_sig_BD),                  
        .sig_except(CP0_sig_except),              
        .src_exccode(CP0_exccode),       
        .cp0_addr_A(instr_m1_A[15:11]),        
        .cp0_addr_B(instr_m1_B[15:11]),        
        .sig_wen_A(cp0_wen_m1_A),               
        .sig_wen_B(cp0_wen_m1_B),               
        .src_wdata_A(mf_rt_m1_A),      
        .src_wdata_B(mf_rt_m1_B), 
        .except_AB_exception(CP0_except_AB),     
        .src_rdata_A(cp0_rdata_m1_A),
        .src_rdata_B(cp0_rdata_m1_B), 
        .src_epc(cp0_epc),
        .sig_intreq(sig_intreq),
        .except_AB(except_AB)
    );

    wire dm_ren_m1;
    wire dm_wen_m1;
    wire [31:0] dm_rt_m1;
    wire [1:0] dm_s_sel;

    DM_F2T1(
        .sig_intreq(sig_intreq),
        .except_AB(except_AB),
        .ren_A(dm_ren_m1_A),
        .wen_A(dm_wen_m1_A),
        .addr_A(ao_m1_A),
        .wadata_A(mf_rt_m1_A),
        .s_sel_A(s_sel_m1_A),

        .ren_B(dm_ren_m1_B),
        .wen_B(dm_wen_m1_B),
        .addr_B(ao_m1_B),
        .wadata_B(mf_rt_m1_B),
        .s_sel_B(s_sel_m1_B),

        .ren(dm_ren_m1),
        .wen(dm_wen_m1),
        .addr(data_sram_addr),
        .wadata(dm_rt_m1),
        .dm_s_sel(dm_s_sel)
    );

    assign data_sram_en = dm_ren_m1 || dm_wen_m1;

    BE be(
        .src_addr(data_sram_addr[1:0]),
        .sig_mf_rt_m(dm_rt_m1),
        .sig_s_sel(dm_s_sel),
        .src_wdata(data_sram_wdata),
        .sig_be(data_sram_wen)
    );

    wire [2:0] ld_ctr_m2_A;
    REGS_FLOW M1_M2_A(
        .clk(clk),
        .reset(reset),
        .stall(stall_m2),
        .clr(clr_m2_A),
        .pc_I(pc_m1_A),
        .pc4_I(pc4_m1_A),
        .pc8_I(pc8_m1_A),
        .ir_I(instr_m1_A),
        .ao_I(ao_m1_A),
        .wd3_sel_I(wd3_sel_m1_A),
        .reg_wen_I(reg_wen_m1_A),
        .data_hazard_I(data_hazard_m1_A),
        .grf_waddr_I(grf_waddr_m1_A),
        .ld_ctr_I(ld_ctr_m1_A),
        .isir_I(isir_m1_A),
        .cp0_grf_I(cp0_rdata_m1_A),

        .pc_O(pc_m2_A),
        .pc4_O(pc4_m2_A),
        .pc8_O(pc8_m2_A),
        .ir_O(ir_m2_A),
        .ao_O(ao_m2_A),
        .wd3_sel_O(wd3_sel_m2_A),
        .reg_wen_O(reg_wen_m2_A),
        .data_hazard_O(data_hazard_m2_A),
        .grf_waddr_O(grf_waddr_m2_A),
        .ld_ctr_O(ld_ctr_m2_A),
        .isir_O(isir_m2_A),
        .cp0_grf_O(cp0_grf_m2_A)
    );

    LD_EXT ld_ext_A(
        .addr10(ao_m2_A[1:0]),
        .src_rdata_dm(data_sram_rdata),
        .sig_ld_opt(ld_ctr_m1_A),
        .rdata(dm_rdata_A)
    );

    wire [2:0] ld_ctr_m2_B;
    REGS_FLOW M1_M2_B(
        .clk(clk),
        .reset(reset),
        .stall(stall_m2),
        .clr(clr_m2_B),
        .pc_I(pc_m1_B),
        .pc4_I(pc4_m1_B),
        .pc8_I(pc8_m1_B),
        .ir_I(instr_m1_B),
        .ao_I(ao_m1_B),
        .wd3_sel_I(wd3_sel_m1_B),
        .reg_wen_I(reg_wen_m1_B),
        .data_hazard_I(data_hazard_m1_B),
        .grf_waddr_I(grf_waddr_m1_B),
        .ld_ctr_I(ld_ctr_m1_B),
        .isir_I(isir_m1_B),
        .cp0_grf_I(cp0_rdata_m1_B),

        .pc_O(pc_m2_B),
        .pc4_O(pc4_m2_B),
        .pc8_O(pc8_m2_B),
        .ir_O(ir_m2_B),
        .ao_O(ao_m2_B),
        .wd3_sel_O(wd3_sel_m2_B),
        .reg_wen_O(reg_wen_m2_B),
        .data_hazard_O(data_hazard_m2_B),
        .grf_waddr_O(grf_waddr_m2_B),
        .ld_ctr_O(ld_ctr_m2_B),
        .isir_O(isir_m2_B),
        .cp0_grf_O(cp0_grf_m2_B)
    );

    LD_EXT ld_ext_B(
        .addr10(ao_m2_B[1:0]),
        .src_rdata_dm(data_sram_rdata),
        .sig_ld_opt(ld_ctr_m1_B),
        .rdata(dm_rdata_B)
    );

endmodule
