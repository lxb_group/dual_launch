`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/10 09:40:21
// Design Name: 
// Module Name: F1_PIPE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module F_STAGE(
    input clk, 
    input reset,
    input eret,
    input intreq,
    input stall_npc,
    input stall_f2,
    input stall_fifo,
    input clr_f2,
    input clr_fifo,
    //NPC
    input [31:0]    pc4_d2_A,
    input [31:0]    instr_d2_A,
    input [31:0]    mf_rs_e_A,
    input [31:0]    pc_bp_error,
    input           bp_error,
    input [1:0]     npc_opt_d1,
    input [31:0]    epc,
    input           JR_e_A,
    output [31:0]   pc_f1,
    //FIFO
    input [31:0]    inst_sram_rdata,
    input [31:0]    ren_A,
    input [31:0]    ren_B,

    output [31:0]   pc_f2,
    output isir_f2,

    output          NPC_jump_branch_d1,
    output          NPC_jump_branch_e,

    output [31:0]   FIFO_instr_A,
    output [31:0]   FIFO_instr_B,
    output [31:0]   FIFO_pc_A,
    output [31:0]   FIFO_pc_B,
    output FIFO_full,
    output FIFO_empty,
    output FIFO_single
    );

    wire [31:0] PC_nom_pc;
    wire [31:0] NPC_pc_f1, pc4_f1;
    wire [31:0] FIFO_instr_A, FIFO_instr_B;

    assign pc4_f1 = NPC_pc_f1 + 4;

    PC pc(
        .clk(clk),
        .reset(reset),
        .src_pc4_f1(pc4_f1),

        .src_nom_pc(PC_nom_pc)
    );

    NPC npc(
        .eret(eret),
        .sig_intreq(intreq),
        .sig_stall(stall_npc),
        .src_nom_pc(PC_nom_pc),
        .src_pc4_d2_A(pc4_d2_A),
        .src_ir26_d2(instr_d2_A[25:0]),
        .src_rf_rs_E(mf_rs_e_A),
        .src_pc_bp(pc_bp_error),
        .sig_bp_wr(bp_error),
        .sig_npc_opt_d1(npc_opt_d1),
        .src_pc_f2(pc_f2),
        .src_epc(epc),
        .sig_jr_e(JR_e_A),
        
        .src_npc(NPC_pc_f1),        
        .NPC_jump_branch_d1(NPC_jump_branch_d1),
        .NPC_jump_branch_e(NPC_jump_branch_e)
    );

    REGS_FLOW F1_F2(
        .clk(clk),
        .reset(reset),
        .stall(stall_f2),
        .clr(clr_f2),
        .pc_I(NPC_pc_f1),
        .isir_I(1'b1),

        .pc_O(pc_f2),
        .isir_O(isir_f2),
    );

    FIFO fifo(
        .clk(clk),
        .reset(reset),
        .clr(clr_fifo),
        .stall(stall_fifo),
        .src_pc(pc_f2),
        .src_instr(inst_sram_rdata),
        .wen(isir_f2),
        .ren_A(ren_A),
        .ren_B(ren_A),    

        .src_instr_A(FIFO_instr_A),
        .src_instr_B(FIFO_instr_B),
        .src_pc_A(FIFO_pc_A),
        .src_pc_B(FIFO_pc_B),
        .sig_full(FIFO_full),
        .sig_empty(FIFO_empty),
        .sig_single(FIFO_single)
    );

endmodule
