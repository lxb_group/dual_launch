`timescale 1ns / 1ps
`include "global.h"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/03 19:16:43
// Design Name: 
// Module Name: BPU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`define S0 2'b00            //不是分支
`define S1 2'b01            //预测结果为跳转
`define S2 2'b10            //预测结果为不跳

`define SNT 2'b00           //strong not taken
`define WNT 2'b01           //weak not taken
`define WT 2'b10            //weak taken
`define ST 2'b11            //strong taken

module BPU(
    input clk,
    input reset,
    input clr,
    input stall,
    input sig_br_d1_A,                    //D1是否为分支指令   
    input src_res_cmp_E_A,              //E级A流水的比较结果，因为分支指令只会出现在A
    input [31:0] src_pc4_d1_A,           //D1级A流水PC
    input [15:0] src_ir16_d1_A,
    output src_bp,                      //预测结果，为1则跳转
    output reg [31:0] src_pc_bp_e,      //分支出错时应该跳转的PC
    output sig_bp_error
    );

    reg[1:0] state_d2, state_e, dbp;
    reg[31:0] pc_bp_d2;
    wire pc_not_branch_d1, pc_branch_d1;

    assign src_bp = sig_br && dbp[1];               //send to ctr, 1 => branch
    assign sig_bp_error = ((src_res_cmp_E_A && (state_e == `S2)) || (!src_res_cmp_E_A && (state_e == `S1)));

    assign pc_not_branch_d1 = src_pc4_d1_A + 4;
    assign pc_branch_d1 = src_pc4_d1_A + {{14{src_ir16_d1_A[15]}}, src_ir16_d1_A, 2'b00};


    always @(posedge clk) begin
        if (reset || clr) begin
            state_d2 <= `S0;
            state_e <= `S0;
            pc_bp_d2 <= 0;
            dbp <= `WT;
            src_pc_bp_e <= 0;
        end
        else if(!stall) begin
            {state_e, src_pc_bp_e} <= {state_d2, pc_bp_d2};
            if (sig_br_d1_A) begin
                {state_d2, pc_bp_d2} <= dbp[1]? {`S1, pc_not_branch_d1} : {`S2, pc_branch_d1};
            end
            else begin
                state_d2 <= `S0;
                pc_bp_d2 <= 0;
            end
            if (state_e != `S0) begin
                if (src_res_cmp_E_A && (dbp != `ST)) begin
                    dbp <= dbp + 2'b01;
                end
                else if (!src_res_cmp_E_A && (dbp != `SNT)) begin
                    dbp <= dbp - 2'b01;
                end
            end 
        end
    end

endmodule
