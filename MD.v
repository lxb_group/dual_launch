`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/09 11:29:10
// Design Name: 
// Module Name: MD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MD(
    input clk,
    input reset,
    input clr,
    input sig_stall,
    input sig_start,
    input [2:0] md_op,
    input [31:0] opt1,
    input [31:0] opt2,
    output [31:0] HI_LO,
    output BUSY
    );

    wire m_axis_dout_tvalid, divs, start;
	wire[79:0] m_axis_dout_tdata;
	wire[32:0] usignedA, usignedB, signedA, signedB, A, B;
	wire[65:0] P;
	assign divs = md_op == `DIV || md_op == `DIVU;
	assign usignedA = {1'b0 , opt1};
	assign usignedB = {1'b0, opt2};
	assign signedA = {opt1[31], opt1};
	assign signedB = {opt2[31], opt2};
	assign {A, B} = (md_op[0])? {usignedA, usignedB}:
								   {signedA, signedB};

    assign start = !clr && !sig_stall && sig_start;

	mult_gen_0 MULT (
  		.CLK(clk),    // input wire CLK
  		.A(A),        // input wire [32 : 0] A
  		.B(B),        // input wire [32 : 0] B
  		.CE(BUSY || start),      // input wire CE
  		.SCLR(clr || reset),  // input wire SCLR
  		.P(P)        // output wire [65 : 0] P
	);

	divisor DIV (
		.aclk(clk),                                      // input wire aclk
		.s_axis_divisor_tvalid(start && divs),    // input wire s_axis_divisor_tvalid
		.s_axis_divisor_tdata({{7{B[32]}}, B}),      // input wire [39 : 0] s_axis_divisor_tdata
		.s_axis_dividend_tvalid(start && divs),  // input wire s_axis_dividend_tvalid
		.s_axis_dividend_tdata({{7{A[32]}}, A}),    // input wire [39 : 0] s_axis_dividend_tdata
		.m_axis_dout_tvalid(m_axis_dout_tvalid),          // output wire m_axis_dout_tvalid
		.m_axis_dout_tdata(m_axis_dout_tdata)            // output wire [79 : 0] m_axis_dout_tdata
	);

	integer timeOfCal;
	reg[31:0] HI, LO;
	assign BUSY = (timeOfCal > 0);
    assign HI_LO = (md_op == `MFHI)? HI:
                (md_op == `MFLO)? LO:
                                  0; 
	

	always@(posedge clk) begin
		if(reset || clr) begin
			timeOfCal = 0;
			HI <= 0;
			LO <= 0;
		end
		else if(start) begin
			case(md_op)
				`MULT: begin
						timeOfCal = 2;
				end
				`MULTU: begin
						timeOfCal = 2;
					end
				`DIV: begin
						timeOfCal = 17;
					end
				`DIVU: begin
						timeOfCal = 17;
					end
				`MTHI: 
					HI <= A;
				`MTLO:
					LO <= A;
			endcase
		end
		else if(timeOfCal > 0) begin
			if(timeOfCal == 1) begin
				{HI, LO} = !m_axis_dout_tvalid? P[63:0]:
	{m_axis_dout_tdata[31:0], m_axis_dout_tdata[71:40]};
			end
			timeOfCal = timeOfCal - 1;
		end
	end

endmodule
