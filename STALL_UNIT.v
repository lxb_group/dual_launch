`timescale 1ns / 1ps
`include "global.h"
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2020/07/09 14:29:28
// Design Name: 
// Module Name: STALL_UNIT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
//pipeline, bpu, fifo, md, pc

//hazard stall: always in E_PIPE, stall: pc, md, bpu, pipeline f2->e
//md_stall: same as up
//intreq: pc <- kernel_addr; clear: d1-m1, bpu, fifo; stall: md
//eret: pc <- epc; clear: d1-m1, bpu, fifo; stall: md
//if branch or jump succeed in d2, clear: fifo, d2; if d2 not stall
//if branch or jump succeed in e, clear: fifo, d2, e; if e not stall
//if eret or intreq, which is jump succeed in m1, clear: fifo, d2, e, m1

module STALL_UNIT(
    input hazard_stall,
    input sig_intreq,
    input except_AB,
    input eret,
    input jump_branch_d1,
    input jump_branch_e,
    input bp_error,
    input fifo_full,

    output sig_stall_npc,
    output sig_stall_fifo,
    output sig_stall_bpu,
    output sig_stall_md,

    output sig_stall_f2,
    output sig_stall_d2,
    output sig_stall_e,
    output sig_stall_m1,
    output sig_stall_m2,
    output sig_stall_w,

    output sig_clr_fifo,
    output sig_clr_bpu,

    output sig_clr_f2,
    output sig_clr_d2,
    output sig_clr_e,
    output sig_clr_m1,
    output sig_clr_m2_A,
    output sig_clr_m2_B,
    output sig_clr_w
    );

    assign bj_succeed_d1 = jump_branch_d1 && !sig_stall_fifo && !bj_succeed_e;
    assign bj_succeed_e = (jump_branch_e || bp_error) && !sig_stall_e;

//hazard_stall
    assign sig_stall_npc = (hazard_stall || fifo_full) && !sig_intreq && !eret;
    assign sig_stall_fifo = hazard_stall;
    assign sig_stall_bpu = hazard_stall;
    assign sig_stall_md = hazard_stall || sig_intreq || eret;

    assign sig_stall_f2 = hazard_stall;
    assign sig_stall_d2 = hazard_stall;
    assign sig_stall_e = hazard_stall;
    assign sig_stall_m1 = 1'b0;
    assign sig_stall_m2_A = 1'b0;
    assign sig_stall_m2_B = 1'b0;
    assign sig_stall_w = 1'b0;
//sig_intreq
    assign sig_clr_fifo = sig_intreq || eret || bj_succeed_d1 || bj_succeed_e;
    assign sig_clr_bpu = sig_intreq || eret;

    assign sig_clr_f2 = 1'b0;
    assign sig_clr_d2 = sig_intreq || eret || bj_succeed_e;
    assign sig_clr_e = sig_intreq || eret || bj_succeed_e;
    assign sig_clr_m1 = hazard_stall || sig_intreq || eret;
    assign sig_clr_m2_A = sig_intreq && (except_AB == `STREAM_A);
    assign sig_clr_m2_B = sig_intreq;
    assign sig_clr_w = 1'b0;
    


endmodule
